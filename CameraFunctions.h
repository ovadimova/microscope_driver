struct BeamParameters
{
	int imax;
	int jmax;
	double dx;
	double dy;
	double xmax;
	double ymax;
	double xdisp;
	double ydisp;
	double intensity;
	int i0;
	int j0;

	BeamParameters();
	BeamParameters(const BeamParameters& obj);
	void SetCenteringPoint(int ic, int jc);
	int Distance();
	int MinDistance();
	int MaxDistance();
	int XDistance();
	int YDistance();

	BeamParameters& operator=(BeamParameters &obj)
	{
		imax = obj.imax;
		jmax = obj.jmax;
		dx = obj.dx;
		dy = obj.dy;
		xmax = obj.xmax;
		ymax = obj.ymax;
		xdisp = obj.xdisp;
		ydisp = obj.ydisp;
		i0 = obj.i0;
		j0 = obj.j0;
		intensity = obj.intensity;
		return *this;
	}
	
	friend bool operator==(const BeamParameters& left, const BeamParameters& right);
	friend bool operator!=(const BeamParameters& left, const BeamParameters& right);
	friend bool operator>(const BeamParameters& left, const BeamParameters& right);
	friend bool operator<(const BeamParameters& left, const BeamParameters& right);
	friend bool operator<=(const BeamParameters& left, const BeamParameters& right);
	friend bool operator>=(const BeamParameters& left, const BeamParameters& right);
};

void getImageFromCamera(unsigned char *pixels, int *width_canvas, int *height_canvas, BeamParameters *statData, int exposition, int mode);

void resizeImageSimple(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v);

void resizeImageByMaxIntensity(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v);

void resizeImageCentered(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v);

void getShiftedPointsCoordinateCentered(int ix, int iy, int *ox, int *oy, int isize_h, int isize_v, int osize_h, int osize_v);

void getShiftedPointsCoordinateSimple(int ix, int iy, int *ox, int *oy, int isize_h, int isize_v, int osize_h, int osize_v);

void resizeImageAtSpecifiedLocation(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v, int x0, int y0);

void smoothingImageDistribution(unsigned char *in, unsigned char *out, int size_h, int size_v, int smoothParam);

void getPositionOfDistributionsMaximum(unsigned char *dist,int size_h, int size_v, int *max_h, int *max_v);

void getPositionOfDistributionMaximumImproved(unsigned char *dist, int size_h, int size_v, int *max_h, int *max_v);

void getNewImageSize(int isize_h, int isize_v, int osize_h, int osize_v, int *nsize_h, int *nsize_v);

void getExpositionLimits(int *minExp, int *maxExp, bool outside);

void getDistributionParameters(unsigned char *pixels, int size_h, int size_v, BeamParameters *result);

double getIntegratedIntensity(unsigned char* dist, int size_h, int size_v);

int findOverLightedAreas(unsigned char* dist, int size_h, int size_v);

void normalizePicture(unsigned char* dist, unsigned char *dist_norm, int size_h, int size_v);