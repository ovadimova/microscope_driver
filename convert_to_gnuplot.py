import scipy.misc
import numpy as np
from numpngw import write_png
file_name = "20171102_scan_0_large.dat"

nx = 0
ny = 0
data = []

with open(file_name, 'r') as infile:
    for line in infile:
        tdata = [int(x) for x in line.split()]
        nx += 1
        ny = len(tdata)
        data.extend(tdata)

file_name_out = "test.dat"
print(nx // 3, ny // 7)

with open(file_name_out, 'w') as outfile:
    for i in range(0, nx):
        if i % 2 == 0:
            for j in range(0, ny):
                if j % 2 == 0:
                    outfile.write(str(i) + '\t' + str(j) + '\t' + str(data[i * ny + j]) + '\n')
            outfile.write('\n')
img_nx = nx // 3
img_ny = ny // 7

ind = 0
for pi in range(0,3):
    for pj in range(0,7):
        #print(pi, pj, ind)
        small_img = [0] * (img_nx * img_ny)
        img_name = 'small_' + str(ind) + '.png'
        for i in range(0, img_nx):
            for j in range(0, img_ny):
                try:
                    small_img[i * img_ny + j] = data[pi * ny * img_nx + pj * img_ny + i * ny + j]
                except Exception:
                    print(pi, pj, i, j)
                    print(len(data), pi * ny * img_nx + pj * img_nx * img_ny + i * ny + j)
        img_data = np.array(small_img, dtype=np.uint8).reshape(img_nx, img_ny)
        print(img_data.ndim)
        print(img_data[img_nx // 3][img_ny // 4])   
        write_png(img_name, img_data, bitdepth=8)
        ind += 1