import numpy as np
from numpngw import write_png
import datetime

today = datetime.datetime.now()
strtoday = today.strftime("%Y%m%d")

file_name_base = "Debug\\" + strtoday + "_scan_"

steps_x = 3
steps_y = 2
total_nx = steps_x * 1392
total_ny = steps_y * 1032
total_img = np.zeros(total_nx * total_ny, dtype=np.uint8).reshape(total_nx, total_ny)
ind = 0
i = 0
while i < steps_x:
    for j in range(0, steps_y):
        nx = 0
        ny = 0
        img_data = []
        file_name = file_name_base + str(ind) + "_small.dat"
        with open(file_name, "r") as fin:
            for line in fin:
                tlist = [int(x) for x in line.split()]
                ny = len(tlist)
                nx += 1
                img_data.extend(tlist)
        print("direct:", ind, i, j, nx, ny)
        img_data_t = [0] * (nx * ny)
        for it in range(0, nx):
            for jt in range(0, ny):
                img_data_t[jt * nx + it] = img_data[it * ny + jt]
                #total_img[i * nx * total_ny + j * ny + it * total_ny + jt] = img_data[it * ny + jt]
        img_to_output = np.array(img_data, dtype=np.uint8).reshape(nx, ny)
        test_img = np.fliplr(img_to_output)
        for it in range(0, nx):
            for jt in range(0, ny):
                total_img[i * nx + it][j * ny + jt] = test_img[it][jt]
        img_name = "scan_" + str(ind) + ".png"
        write_png(img_name, img_to_output, bitdepth=8)
        ind += 1
    i += 1
    if (i < steps_x):
        for j in range(1, -1, -1):
            nx = 0
            ny = 0
            img_data = []
            file_name = file_name_base + str(ind) + "_small.dat"
            with open(file_name, "r") as fin:
                for line in fin:
                    tlist = [int(x) for x in line.split()]
                    ny = len(tlist)
                    nx += 1
                    img_data.extend(tlist)
            
            print("rev:", ind, i, j, nx, ny)
            img_to_output = np.array(img_data, dtype=np.uint8).reshape(nx, ny)
            test_img = np.fliplr(img_to_output)
            for it in range(0, nx):
                for jt in range(0, ny):
                    total_img[i * nx + it][j * ny + jt] = test_img[it][jt]
            img_name = "scan_" + str(ind) + ".png"
            write_png(img_name, img_to_output, bitdepth=8)
            ind += 1
        i += 1
#total_img_to_output = np.array(total_img, dtype=np.uint8).reshape(total_nx, total_ny).transpose()
print(total_img.dtype)
#np.uint8(total_img)
write_png("total.png", total_img, bitdepth=8)

# Total image by the program

file_total_picture_name = file_name_base + "0_large.dat"
tlarge_array = []
nx_l = 0
ny_l = 0
with open(file_total_picture_name, "r") as file:
    for line in file:
        tlarge_array.extend([int(x) for x in line.split()])
        nx_l += 1

ny_l = len(tlarge_array) // nx_l

total_img_to_output = np.array(tlarge_array, dtype=np.uint8).reshape(nx_l, ny_l).transpose()
write_png("total_by_soft.png", total_img_to_output, bitdepth=8)