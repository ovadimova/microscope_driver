#include "stdafx.h"

bool operator==(const BeamParameters& left, const BeamParameters& right)
{
	int ax = (left.imax - left.i0) * (left.imax - left.i0);
	int ay = (left.jmax - left.j0) * (left.jmax - left.j0);
	int bx = (right.imax - right.i0) * (right.imax - right.i0);
	int by = (right.jmax - right.j0) * (right.jmax - right.j0);

	if ((ax + ay) == (bx + by))
	{
		return true;
	}
	
	return false;
}

bool operator!=(const BeamParameters& left, const BeamParameters& right)
{
	return (!(left == right));
}

bool operator>(const BeamParameters& left, const BeamParameters& right)
{
	int ax = (left.imax - left.i0) * (left.imax - left.i0);
	int ay = (left.jmax - left.j0) * (left.jmax - left.j0);
	int bx = (right.imax - right.i0) * (right.imax - right.i0);
	int by = (right.jmax - right.j0) * (right.jmax - right.j0);

	if ((ax + ay) > (bx + by))
	{
		return true;
	}
	return false;
}

bool operator<(const BeamParameters& left, const BeamParameters& right)
{
	int ax = (left.imax - left.i0) * (left.imax - left.i0);
	int ay = (left.jmax - left.j0) * (left.jmax - left.j0);
	int bx = (right.imax - right.i0) * (right.imax - right.i0);
	int by = (right.jmax - right.j0) * (right.jmax - right.j0);

	if ((ax + ay) < (bx + by))
	{
		return true;
	}
	return false;
}

bool operator<=(const BeamParameters& left, const BeamParameters& right)
{
	return (!(left > right));
}

bool operator>=(const BeamParameters& left, const BeamParameters& right)
{
	return (!(left < right));
}

BeamParameters::BeamParameters()
{
	imax = 0;
	jmax = 0;
	xmax = 0.0;
	ymax = 0.0;
	xdisp = 0.0;
	ydisp = 0.0;
	dx = 0.0;
	dy = 0.0;
	i0 = 0;
	j0 = 0;
	intensity = 0.0;
}

BeamParameters::BeamParameters(const BeamParameters& obj)
{
	imax = obj.imax;
	jmax = obj.jmax;
	dx = obj.dx;
	dy = obj.dy;
	xmax = obj.xmax;
	ymax = obj.ymax;
	xdisp = obj.xdisp;
	ydisp = obj.ydisp;
	i0 = obj.i0;
	j0 = obj.j0;
	intensity = obj.intensity;
}
	
void BeamParameters::SetCenteringPoint(int ic, int jc)
{
	i0 = ic;
	j0 = jc;
};

int BeamParameters::Distance()
{
	int result = 0;

	result += (imax - i0) * (imax - i0)
		+ (jmax - j0) * (jmax - j0);

	return result;
}

int BeamParameters::MinDistance()
{
	int dx = (i0 > imax) ? (i0 - imax) : (imax - i0);
	int dy = (j0 > jmax) ? (j0 - jmax) : (jmax - j0);

	return ((dx < dy) ? (dx) : (dy));
}

int BeamParameters::MaxDistance()
{
	int dx = (i0 > imax) ? (i0 - imax) : (imax - i0);
	int dy = (j0 > jmax) ? (j0 - jmax) : (jmax - j0);

	return ((dx > dy) ? (dx) : (dy));
}

int BeamParameters::XDistance()
{
	return ((i0 > imax) ? (i0 - imax) : (imax - i0));
}

int BeamParameters::YDistance()
{
	return ((j0 > jmax) ? (j0 - jmax) : (jmax - j0));
}

void getImageFromCamera(unsigned char *pixels, int *width_canvas, int *height_canvas, BeamParameters *statData, int exposition, int mode)
{
	static unsigned char *data = NULL;
	static unsigned char *tdata = NULL;
	static DWORD roi_size_v = 0;
	static DWORD roi_size_h = 0;
	static char camName[128];
	static DWORD camNum = 0;
	static CSDUCameraDevice *Camera;
	DWORD real_length = 0;
	DWORD status = 0;
	int i = 0;
	int j = 0;
	
	/*** Initialization of Camera ***/
	if (mode == 0)
	{
		Camera = sdu_open_interface(&camNum);
		sdu_open_camera(Camera, static_cast<DWORD>(0), camName);
		sdu_set_trigmode(Camera, SDU_TRIGMODE_SOFTSTART);
		sdu_set_encoding(Camera, SDU_ENCODING_8BPP);
		sdu_set_binning(Camera, SDU_BINNING_1x1);
		sdu_get_roi_size(Camera, &roi_size_h, &roi_size_v);
		data = new unsigned char [roi_size_h * roi_size_v];
		tdata = new unsigned char [roi_size_h * roi_size_v];
		int twidth_canvas = 0;
		int theight_canvas = 0;
		getNewImageSize(static_cast<int>(roi_size_h), static_cast<int>(roi_size_v), *width_canvas, *height_canvas, &twidth_canvas, &theight_canvas);
		*width_canvas = twidth_canvas;
		*height_canvas = theight_canvas;

		DWORD minExp = 0;
		DWORD maxExp = 0;
		int iMinExp = 0;
		int iMaxExp = 0;

		sdu_get_min_exp(Camera, &minExp);
		sdu_get_max_exp(Camera, &maxExp);

		iMinExp = static_cast<int>(minExp);
		iMaxExp = static_cast<int>(maxExp);

		getExpositionLimits(&iMinExp, &iMaxExp, false);
		return;
	}

	/****Close Camera ***/
	if (mode == 2)
	{
		sdu_close_interface(Camera);
		delete [] data;
		delete [] tdata;
		return;
	}

	/****Change the Camera's Exposition*********/
	if (mode == 3)
	{
		sdu_set_exp(Camera, static_cast<DWORD>(exposition));
		return;
	}
	//*****Clearing of the camera's buffer****//
	sdu_fifo_init(Camera);
	//*****Start****//
	sdu_cam_start(Camera);

	while(true)
	{
		sdu_get_status(Camera, &status);
		if (!(status & SDU_STAT_BUSY))
		{
			break;
		}
	}
	
	sdu_get_status(Camera, &status);
	
	if (!(status & SDU_STAT_FIFOEMPTY))
	{
		sdu_read_data(Camera, data, roi_size_h * roi_size_v, &real_length);
	}
	
	for (i = 0; i < static_cast<int>(roi_size_h); i++)
	{
		for(j = 0; j < static_cast<int>(roi_size_v); j++)
		{
			tdata[i * static_cast<int>(roi_size_v) + j] = data[j * static_cast<int>(roi_size_h) + i];
		}
	}
	getDistributionParameters(tdata, static_cast<int>(roi_size_h), static_cast<int>(roi_size_v), statData);
	smoothingImageDistribution(tdata, data, static_cast<int>(roi_size_h), static_cast<int>(roi_size_v), 2);
	resizeImageByMaxIntensity(data, pixels, static_cast<int>(roi_size_h), static_cast<int>(roi_size_v), *width_canvas, *height_canvas);
	/*for (i = 0; i < *width_canvas; i++)
	{
		for( j = 0; j < *height_canvas; j++)
		{
			if (( i < static_cast<int>(roi_size_h)) && (j < static_cast<int>(roi_size_h)))
			{
				pixels[i * (*height_canvas) + j] = data[j * static_cast<int>(roi_size_h) + i] < 255 ? data[j * static_cast<int>(roi_size_h) + i] : 255;
			}
			else
			{
				pixels[ i * (*height_canvas) + j ] = 255;
			}
		}
	}*/
}

void resizeImageSimple(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v)
{
	int i = 0;
	int j = 0;

	int rem_h = isize_h - osize_h;
	int rem_v = isize_v - osize_v;
	int step_h = (rem_h > 0) ? (isize_h / rem_h) : 1;
	int step_v = (rem_v > 0) ? (isize_v / rem_v) : 1;
	int c_h = 1;
	int c_v = 1;
	int ih = 0;
	int iv = 0;
	for (i = 1; i < isize_h; i++)
	{
		c_v = 1;
		iv = 0;
		if ( i <=  c_h * step_h)
		{
			for (j = 1; j < isize_v; j++)
			{
				if (j <= c_v * step_v )
				{
					out[ih * osize_v + iv] = in[i * isize_v + j];
					iv++;
				}
				else
				{
					if (c_v < rem_v)
					{
						c_v++;
					}
					else
					{
						c_v *= 2;
					}
				}
			}
			ih++;
		}
		else
		{
			if (c_h < rem_h)
			{
				c_h++;
			}
			else
			{
				c_h *= 2;
			}
		}
	}
	return;
}

void getShiftedPointsCoordinateSimple(int ix, int iy, int *ox, int *oy, int isize_h, int isize_v, int osize_h, int osize_v)
{
	int i = 0;
	int j = 0;

	int rem_h = isize_h - osize_h;
	int rem_v = isize_v - osize_v;
	int step_h = (rem_h > 0) ? (isize_h / rem_h) : 1;
	int step_v = (rem_v > 0) ? (isize_v / rem_v) : 1;

	*ox = static_cast<int>(ix * static_cast<double>(osize_h) / static_cast<double>(isize_h));//(rem_h > 0) ? (ix - (ix / step_h)) : ix;
	*oy = static_cast<int>(iy * static_cast<double>(osize_v) / static_cast<double>(isize_v));;//(rem_v > 0) ? (iy - (iy / step_v)) : iy;

}

void resizeImageByMaxIntensity(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v)
{
	

	int max_h = 0;
	int max_v = 0;

	getPositionOfDistributionsMaximum(in, isize_h, isize_v, &max_h, &max_v);
	
	resizeImageAtSpecifiedLocation(in, out, isize_h, isize_v, osize_h, osize_v, max_h, max_v);

	return;
}


void resizeImageCentered(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v)
{
	int x0 = isize_h / 2;
	int y0 = isize_v / 2;

	resizeImageAtSpecifiedLocation(in, out, isize_h, isize_v, osize_h, osize_v, x0, y0);

	return;
}

void getShiftedPointsCoordinateCentered(int ix, int iy, int *ox, int *oy, int isize_h, int isize_v, int osize_h, int osize_v)
{
	int left = (isize_h - osize_h) / 2;
	int up = (isize_v - osize_v) / 2;
	
	*ox = ix - left;
	*oy = iy - up;
}

void resizeImageAtSpecifiedLocation(unsigned char *in, unsigned char *out, int isize_h, int isize_v, int osize_h, int osize_v, int x0, int y0)
{
	int i = 0;
	int j = 0;
	int io = 0;
	int jo = 0;
	int sx = 0;
	int sy = 0;
	int fx = 0;
	int fy = 0;
	int ldx = x0 - osize_h / 2 + osize_h % 2;
	int rdx = x0 + osize_h / 2;
	int udy = y0 - osize_v / 2 + osize_v % 2;
	int bdy = y0 + osize_v / 2;
	
	if ((ldx >= 0) && (rdx < isize_h))
	{
		sx = ldx;
		fx = rdx;
	}
	else if (ldx < 0)
	{
		sx = 0;
		fx = osize_h;
	}
	else
	{
		fx = isize_h;
		sx = isize_h - osize_h;
	}
	
	if ((udy >=0) && (bdy < isize_v))
	{
		sy = udy;
		fy = bdy;
	}
	else if (udy < 0)
	{
		sy = 0;
		fy = osize_v;
	}
	else
	{
		fy = isize_h;
		sy = isize_h - osize_h;
	}

	for (i = sx; i < fx; i++)
	{
		jo = 0;
		for(j = sy; j < fy; j++)
		{
			out[io * osize_v + jo] = in[i * isize_v + j];
			jo++;
		}
		io++;
	}

	return;
}


void smoothingImageDistribution(unsigned char *in, unsigned char *out, int size_h, int size_v, int smoothParam)
{
	int cSmooth = smoothParam < 10 ? smoothParam : 10;
	int i = 0;
	int j = 0;

	int area = 2 * cSmooth * (cSmooth + 1) + 1;

	for (i = cSmooth; i < size_h - cSmooth; i++)
	{
		for (j = cSmooth; j < size_v - cSmooth; j++)
		{
			int avInt = 0;
			int p = 0;
			for (p = -cSmooth; p < cSmooth + 1; p++)
			{
				int q = cSmooth - abs(p);
				int t = 0;
				for (t = - q; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
				}
			}
			out[ i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}
		
		for (j = 0; j < cSmooth; j++)
		{
			int avInt = 0;
			int t_Ar = 0;
			int p = 0;
			for (p = -j; p < cSmooth + 1; p++)
			{
				int q = cSmooth - abs(p);
				int t = 0;
				for (t = -q; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + t) * size_v + (j + p)]);
					t_Ar++;
				}
			}
			out[ i * size_v + j] = static_cast<unsigned char>(avInt / t_Ar);
		}
		
		for (j = size_v - cSmooth; j < size_v; j++)
		{
			int avInt = 0;
			int t_Ar = 0;
			int p = 0;
			for( p = -cSmooth; p < (size_v - j); p++)
			{
				int q =cSmooth - abs(p);
				int t = 0;
				for (t = -q; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + t) * size_v + (j + p)]);
					t_Ar++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt / t_Ar);
		}
	}

	for (i = 0; i < cSmooth; i++)
	{
		for (j = 0; j < cSmooth; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;
			for (p = -i; p < cSmooth + 1; p++)
			{
				int q = cSmooth - abs(p);
				int qmin = (j < q) ? j : q;
				int t = 0;
				for (t = -qmin; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
					area++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}

		for (j = cSmooth; j < size_v - cSmooth; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;
			for (p = -i; p < cSmooth + 1; p++)
			{
				int q = cSmooth - abs(p);
				int t = 0;
				for( t = -q; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
					area++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}

		for (j = size_v - cSmooth; j < size_v; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;
			for(p = -i; p < cSmooth + 1; p++)
			{
				int q = cSmooth - abs(p);
				int qmax = (q < (size_v - j)) ? q : (size_v - j);
				int t = 0;
				for(t = -q; t < qmax + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
					area++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt /area);
		}
	}

	for (i = size_h - cSmooth; i < size_h; i++)
	{
		for (j = 0; j < cSmooth; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;

			for(p = -cSmooth; p < (size_h - i); p++)
			{
				int q = cSmooth - abs(p);
				int qmin = (j < q) ? j : q;
				int t = 0;
				for(t = -qmin; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
					area++;
				}
			}

			out[i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}

		for (j = cSmooth; j < size_v - cSmooth; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;
			for(p = -cSmooth; p < (size_h - i); p++)
			{
				int q = cSmooth - abs(p);
				int t = 0;
				for( t = -q; t < q + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + (j + t)]);
					area++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}

		for(j = size_v - cSmooth; j < size_v; j++)
		{
			area = 0;
			int p = 0;
			int avInt = 0;
			for (p = -cSmooth; p < (size_h - i); p++)
			{
				int q = cSmooth - abs(p);
				int qmax = (q < (size_v - j)) ? q : (size_v - j);
				int t = 0;
				for (t = -q; t < qmax + 1; t++)
				{
					avInt += static_cast<int>(in[(i + p) * size_v + j]);
					area++;
				}
			}
			out[i * size_v + j] = static_cast<unsigned char>(avInt / area);
		}
	}
	return;
}

void getPositionOfDistributionsMaximum(unsigned char *dist,int size_h, int size_v, int *max_h, int *max_v)
{
	*max_h = size_h / 2;
	*max_v = size_v / 2;
	int i = 0;
	int j = 0;
	double idist = 0.0;
	double iidist = 0.0;
	double ijdist = 0.0;
	int maxD = 0;
	double *tdist = new double [size_v * size_h];

	for(i = 0; i < size_h;  i++)
	{
		for(j = 0; j < size_v; j++)
		{
			if (static_cast<int>(dist[i * size_v + j]) > maxD)
			{
				maxD = static_cast<int>(dist[i * size_v + j]);
			}
		}
	}
	
	for (i = 0; i < size_h; i++)
	{
		for(j = 0; j < size_v; j++)
		{
			tdist[i * size_v + j] = static_cast<double>(dist[i * size_v + j]) / static_cast<double>(maxD);
		}
	}

	for (i = 0; i < size_h; i++)
	{
		double tidist = 0.0;
		double tiidist = 0.0;
		double tijdist = 0.0;

		for(j = 0; j < size_v; j++)
		{
			tidist += tdist[i * size_v + j];
			tiidist += static_cast<double>(i) * tdist[i * size_v + j];
			tijdist += static_cast<double>(j) * tdist[i * size_v + j];
		}
		idist += tidist;
		iidist += tiidist;
		ijdist += tijdist;
	}

	*max_h = static_cast<int>(iidist / idist);
	*max_v = static_cast<int>(ijdist / idist);

	delete [] tdist;
	return;
}

void getNewImageSize(int isize_h, int isize_v, int osize_h, int osize_v, int *nsize_h, int *nsize_v)
{
	if ((isize_h <= osize_h) && (isize_v <= osize_v))
	{
		*nsize_h = isize_h;
		*nsize_v = isize_v;
		return;
	}
	
	if ((isize_h > osize_h) && (isize_v <= osize_v))
	{
		*nsize_h = osize_h;
		*nsize_v = static_cast<int>(isize_v * static_cast<double>(osize_h) / static_cast<double>(isize_h));
		return;
	}

	if ((isize_h <= osize_h) && (isize_v > osize_v))
	{
		*nsize_h = static_cast<int>(isize_h * static_cast<double>(osize_v) / static_cast<double>(isize_v));
		*nsize_v = osize_v;
		return;
	}

	double av = static_cast<double>(osize_v) / static_cast<double>(isize_v);
	double ah = static_cast<double>(osize_h) / static_cast<double>(isize_h);
	
	if (av > ah)	
	{
		*nsize_h = osize_h;
		*nsize_v = static_cast<int>(isize_v * static_cast<double>(osize_v) / static_cast<double>(isize_h));
	}
	else
	{
		*nsize_h = static_cast<int>(isize_h * static_cast<double>(osize_v) / static_cast<double>(isize_v));
		*nsize_v = osize_v;
	}

	return;
}

void getExpositionLimits(int *minExp, int *maxExp, bool outside)
{
	static int minimalExposition = 0;
	static int maximalExposition = 0;

	if (outside)
	{
		*minExp = minimalExposition;
		*maxExp = maximalExposition;
		return;
	}

	minimalExposition = *minExp;
	maximalExposition = *maxExp;
	return;
}

void getDistributionParameters(unsigned char *pixels, int size_h, int size_v, BeamParameters *result)
{
	double *normDist = new double[size_h * size_v];
	int i = 0;
	int j = 0;
	
	getPositionOfDistributionMaximumImproved(pixels, size_h, size_v, &i, &j);
	result->imax = i;
	result->jmax = j;
	result->xmax = result->imax * result->dx;
	result->ymax = result->jmax * result->dy;
	
	double maxD = (pixels[result->imax * size_v + result->jmax] != 0) ? (1.0 / static_cast<double>(pixels[result->imax * size_v + result->jmax])) : 1.0;
	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			normDist[i * size_v + j] = static_cast<double>(pixels[i * size_v + j]) * maxD;
		}
	}

	double ddx = 0.0;
	double ddy = 0.0;
	double ddi = 0.0;

	for (i = 0; i < size_h; i++)
	{
		double tdx = 0.0;
		double tdy = 0.0;
		double tdi = 0.0;

		for (j = 0; j < size_v; j++)
		{
			double c = normDist[ i * size_v + j];
			tdx += c * static_cast<double>((i - result->imax) * (i - result->imax));
			tdy += c * static_cast<double>((j - result->jmax) * (j - result->jmax));
			tdi += c;
		}
		ddx += tdx;
		ddy += tdy;
		ddi += tdi;
	}

	result->xdisp = sqrt(ddx / ddi) * result->dx;
	result->ydisp = sqrt(ddy / ddi) * result->dy;

	delete [] normDist;

	return;
}

double getIntegratedIntensity(unsigned char* dist, int size_h, int size_v)
{
	double result = 0.0;
	int i = 0;
	int j = 0;
	double *tempDist = new double[size_h * size_v];
	

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			tempDist[i * size_v + j] = static_cast<double>(dist[i * size_v + j]) / 256.0;
		}
	}

	for (i = 0; i < size_h; i++)
	{
		double td = 0.0;
		for (j = 0; j < size_v; j++)
		{
			td += tempDist[i * size_v + j];
		}
		result += td;
	}
	delete [] tempDist;
	
	return result;
}

int findOverLightedAreas(unsigned char* dist, int size_h, int size_v)
{
	bool found = false;
	bool highIntFlag = false;
	int *highInt = new int [size_h * size_v];
	int *mask = new int [size_h * size_v];
	int i = 0;
	int j = 0;
	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			highInt[i * size_v + j] = (dist[i * size_v + j] > 240) ? (static_cast<int>(dist[i * size_v + j])) : (0);
			mask[i * size_v + j] = 0;
		}
	}

	int globalIslandIndex = 1;
	int currArea = 0;

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			currArea = 0;
			if ((highInt[i * size_v + j]) && (!mask[i * size_v + j]))
			{
				currArea++;
				highIntFlag = true;
				bool reached = false;
				mask[i * size_v + j] = globalIslandIndex;
				std::vector<std::pair<int, int>> nextPoints;
				std::pair<int, int> tPoint(i, j);
				nextPoints.push_back(tPoint);
				while (!reached)
				{
					int added = 0;
					int nl = nextPoints.size();
					for (int ct = 0; ct < nl; ct++)
					{
						int ic = nextPoints[ct].first;
						int jc = nextPoints[ct].second;
						if ((ic - 1) > -1)
						{
							if ((highInt[(ic - 1) * size_v + jc]) && (!mask[(ic - 1) * size_v + jc]))
							{
								tPoint.first = ic - 1;
								tPoint.second = jc;
								nextPoints.push_back(tPoint);
								mask[(ic - 1) * size_v + jc] = globalIslandIndex;
								currArea++;
								added++;
							}
						}
						if ((ic + 1) < size_h)
						{
							if((highInt[(ic + 1) * size_v + jc]) && (!mask[(ic + 1) * size_v + jc]))
							{
								tPoint.first = ic + 1;
								tPoint.second = jc;
								nextPoints.push_back(tPoint);
								mask[(ic + 1) * size_v + jc] = globalIslandIndex;
								currArea++;
								added++;
							}
						}
						if ((jc - 1) > -1)
						{
							if ((highInt[ic * size_v + jc - 1]) && (!mask[ic * size_v + jc - 1]))
							{
								tPoint.first = ic;
								tPoint.second = jc - 1;
								nextPoints.push_back(tPoint);
								mask[ic * size_v + jc - 1] = globalIslandIndex;
								currArea++;
								added++;
							}
						}
						if ((jc + 1) < size_v)
						{
							if ((highInt[ic * size_v + jc + 1]) && (!mask[ic * size_v + jc + 1]))
							{
								tPoint.first = ic;
								tPoint.second = jc + 1;
								nextPoints.push_back(tPoint);
								mask[ic * size_v + jc + 1] = globalIslandIndex;
								currArea++;
								added++;
							}
						}
						if(currArea > 10)
						{
							i = size_h + 1;
							j = size_v + 1;
							reached = true;
							found = true;
							break;
						}
					}
					nextPoints.erase(nextPoints.begin(), nextPoints.begin() + nl);

					if(added == 0)
					{
						reached = true;
					}
										
				}
				globalIslandIndex++;
			}
		}
	}

	if (!highIntFlag)
	{
		return 2;
	}

	delete [] highInt;
	delete [] mask;

	return static_cast<int>(found);
}

void getPositionOfDistributionMaximumImproved(unsigned char *dist, int size_h, int size_v, int *max_h, int *max_v)
{
	int *highInt = new int [size_h * size_v];
	int *mask = new int [size_h * size_v];
	unsigned char *toBeProcessed = new unsigned char [size_h * size_v];
	int maxInt = 0;
	int cutoffLevel = 0;
	int i = 0;
	int j = 0;

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			mask[i * size_v + j] = 0;
			highInt[i * size_v + j] = 0;
			if (maxInt < static_cast<int>(dist[i * size_v + j]))
			{
				maxInt = static_cast<int>(dist[i * size_v + j]);
			}
		}
	}
	cutoffLevel = static_cast<int>(0.75 * static_cast<double>(maxInt));

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			if(static_cast<int>(dist[i * size_v + j]) >= cutoffLevel)
			{
				highInt[i * size_v + j] = static_cast<int>(dist[i * size_v + j]);
			}
		}
	}

	int globalIslandIndex = 1;
	
	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			if ((highInt[i * size_v + j]) && (!mask[i * size_v + j]))
			{
				bool reached = false;
				mask[i * size_v + j] = globalIslandIndex;
				std::vector<std::pair<int, int>> nextPoints;
				std::pair<int, int> tPoint;

				tPoint.first = i;
				tPoint.second = j;
				nextPoints.push_back(tPoint);

				while(!reached)
				{
					int nl = nextPoints.size();
					bool added = false;
					
					for (int ct = 0; ct < nl; ct++)
					{
						int ic = nextPoints[ct].first;
						int jc = nextPoints[ct].second;

						if ((ic + 1) < size_h)
						{
							if ((highInt[(ic + 1) * size_v + jc]) && (!mask[(ic + 1) * size_v + jc]))
							{
								added = true;
								tPoint.first = ic + 1;
								tPoint.second = jc;
								nextPoints.push_back(tPoint);
								mask[(ic + 1) * size_v + jc] = globalIslandIndex;
							}
						}

						if ((ic - 1) > -1)
						{
							if ((highInt[(ic - 1) * size_v + jc]) && (!mask[(ic - 1) * size_v + jc]))
							{
								added = true;
								tPoint.first = ic - 1;
								tPoint.second = jc;
								nextPoints.push_back(tPoint);
								mask[(ic - 1) * size_v + jc] = globalIslandIndex;
							}
						}

						if ((jc + 1) < size_v)
						{
							if ((highInt[ic * size_v + jc + 1]) && (!mask[ic * size_v + jc + 1]))
							{
								added = true;
								tPoint.first = ic;
								tPoint.second = jc + 1;
								nextPoints.push_back(tPoint);
								mask[ic * size_v + jc + 1] = globalIslandIndex;
							}
						}

						if ((jc - 1) > -1)
						{
							if ((highInt[ic * size_v + jc - 1]) && (!mask[ic * size_v + jc - 1]))
							{
								added = true;
								tPoint.first = ic;
								tPoint.second = jc - 1;
								nextPoints.push_back(tPoint);
								mask[ic * size_v + jc - 1] = globalIslandIndex;
							}
						}

					}
					nextPoints.erase(nextPoints.begin(), nextPoints.begin() + nl);
					if(!added)
					{
						reached = true;
					}
				}
				globalIslandIndex++;
			}
		}
	}

	int *islands = new int [globalIslandIndex];

	for (i = 0; i < globalIslandIndex; i++)
	{
		islands[i] = 0;
	}

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			if (mask[i * size_v + j])
			{
				islands[mask[i * size_v + j] - 1]++;
			}
		}
	}

	int maxIslandArea = 0;
	int maxIslandAreaIndex = 0;
	
	for (i = 0; i < globalIslandIndex; i++)
	{
		if (islands[i] > maxIslandArea)
		{
			maxIslandArea = islands[i];
			maxIslandAreaIndex = i + 1;
		}
	}

	for (i = 0; i < size_h; i++)
	{
		for (j = 0; j < size_v; j++)
		{
			if (mask[i * size_v + j] != maxIslandAreaIndex)
			{
				toBeProcessed[i * size_v + j] = 0;
			}
			else
			{
				toBeProcessed[i * size_v + j] = static_cast<unsigned char>(highInt[i * size_v + j]);
			}
				
		}
	}

	getPositionOfDistributionsMaximum(toBeProcessed, size_h, size_v, max_h, max_v);

	delete [] toBeProcessed;
	delete [] islands;
	delete [] highInt;
	delete [] mask;
}

void normalizePicture(unsigned char *dist, unsigned char *dist_norm, int size_h, int size_v)
{
	int max_h = 0;
	int max_v = 0;

	getPositionOfDistributionsMaximum(dist, size_h, size_v, &max_h, &max_v);

	double max_value = static_cast<double>((dist[max_h * size_v + max_v] > 0) ? (dist[max_h * size_v + max_v]) : (1));
	double mult = 255.0 / max_value;

	for (int i = 0; i < size_h; i++)
	{
		for (int j = 0; j < size_v; j++)
		{
			dist_norm[i * size_h + j] = static_cast<unsigned char>(static_cast<double>(dist[i * size_v + j]) * mult);
		}
	}
}