#pragma once


namespace Microscope_driver {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;
	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			this->Text = L"Microscope Driver";
			RECT desktop;
			const HWND hDesktop = GetDesktopWindow();
			GetWindowRect(hDesktop, &desktop);
			this->_width = desktop.right - 10;
			this->_height = desktop.bottom - 20;

			DWORD camNum = 0;
			char camName[128];
			this->Camera = sdu_open_interface(&camNum);

			this->_camStatus = false;
			this->_camStart = false;
			this->_isDataCanBeRead = false;
			this->_isCalibrated = false;
			this->_processLaunched = 0;
			this->_isImageStored = false;

			if (!camNum)
			{
				this->Height = 480;
				this->Width = 640;
				this->button1->Visible = false;
				this->button2->Visible = false;
				this->button3->Visible = false;

				this->button4->Top = 400;
				this->button4->Left = 370;
				this->button4->Width = 100;
				this->button4->Height = 40;
				this->button4->Text = L"Exit";

				this->button5->Visible = false;
				this->button6->Visible = false;
				this->button7->Visible = false;
				this->button8->Visible = false;
				this->button9->Visible = false;
				this->button10->Visible = false;
				this->button11->Visible = false;
				this->button12->Visible = false;
				this->button13->Visible = false;
				this->button14->Visible = false;
				this->button15->Visible = false;
				this->button16->Visible = false;

				this->numericUpDown1->Visible = false;
				this->numericUpDown2->Visible = false;
				this->numericUpDown3->Visible = false;
				this->numericUpDown4->Visible = false;
				this->numericUpDown5->Visible = false;
				this->numericUpDown6->Visible = false;
				this->numericUpDown7->Visible = false;
				this->numericUpDown8->Visible = false;
				this->groupBox1->Visible = false;
				this->groupBox2->Visible = false;
				this->groupBox3->Visible = false;
				this->groupBox4->Visible = false;
				this->groupBox5->Visible = false;
				this->groupBox6->Visible = false;
				this->groupBox7->Visible = false;
				this->label1->Visible = false;
				this->label2->Visible = false;
				this->label3->Visible = false;
				this->label4->Visible = false;
				this->label6->Visible = false;
				this->label7->Visible = false;
				this->label8->Visible = false;
				this->label9->Visible = false;
				this->label10->Visible = false;
				this->label11->Visible = false;
				this->label12->Visible = false;
				this->label13->Visible = false;
				this->label14->Visible = false;

				this->textBox1->Visible = false;
				this->textBox2->Visible = false;

				this->label5->Top = 100;
				this->label5->Left = 40;
				this->label5->Font = gcnew System::Drawing::Font(this->label1->Font->FontFamily, 16);
				this->label5->Text = L"Please, plug the Camera into the computer\nand restart the application";

				this->radioButton1->Visible = false;
				this->radioButton2->Visible = false;
				this->radioButton3->Visible = false;
				this->radioButton4->Visible = false;
				this->radioButton5->Visible = false;
				this->radioButton6->Visible = false;
				this->radioButton7->Visible = false;
				this->radioButton8->Visible = false;

				this->_camStart = false;
			}
			else
			{
				//this->label6->Text = System::Convert::ToString(this->_width) + L"x" + System::Convert::ToString(this->_height) + L"\n";
				sdu_open_camera(this->Camera, static_cast<DWORD>(0), camName);
				sdu_set_trigmode(this->Camera, SDU_TRIGMODE_SOFTSTART);
				sdu_set_encoding(this->Camera, SDU_ENCODING_8BPP);
				sdu_set_binning(this->Camera, SDU_BINNING_1x1);

				DWORD tROI_h = 0;
				DWORD tROI_v = 0;
				DWORD minPeriod = 0;
	
				sdu_get_roi_size(Camera, &tROI_h, &tROI_v);
				this->_roi_size_h = tROI_h;
				this->_roi_size_v = tROI_v;

				sdu_get_min_period(Camera, &minPeriod);

				this->_minPeriod = static_cast<int>(minPeriod);

				this->data = new unsigned char [static_cast<int>(tROI_h * tROI_v)];
				this->tdata = new unsigned char [static_cast<int>(tROI_h * tROI_v)];
				
				/*Test how everything will look at laptop screen*/
				this->_width = 1366;
				this->_height = 768;

				this->_picWidth = this->_width - 280;
				this->_picHeight = this->_height - 160;

				int tcanvas_h = 0;
				int tcanvas_v = 0;
				
				getNewImageSize(static_cast<int>(tROI_h), static_cast<int>(tROI_v), this->_picWidth, this->_picHeight, &tcanvas_h, &tcanvas_v);

				this->Width = tcanvas_h + 360;
				this->Height = tcanvas_v + 160;
				
				/*To be reconsidered in order to keep sort of ratio*/
				if (this->Width > this->_width)
				{
					this->Width = this->_width;
				}
				if (this->Height > this->_height)
				{
					this->Height = this->_height;
				}

				this->Top = 0;
				this->Left = 0;

				this->_picWidth = tcanvas_h;
				this->pictureBox1->Width = tcanvas_h;
				this->_picHeight = tcanvas_v;
				this->pictureBox1->Height = tcanvas_v;
				this->pictureBox1->Left = 20;
				this->pictureBox1->Top = 60;
				
				this->label5->Top = 12;
				this->label5->Left = 10;
				this->label5->Text = L"Current status: ";

				this->label11->Top = 8;
				this->label11->Left = 200;
				this->label11->Text = L"";
				
				this->groupBox1->Text = L"Scanning parameters";
				this->groupBox1->Left = this->pictureBox1->Left + this->pictureBox1->Width + 10;
				this->groupBox1->Top = this->pictureBox1->Top + 10;
				this->groupBox1->Width = 220;
				this->groupBox1->Height = 140;
				
				this->label1->Text = L"x step size:";
				this->label1->Left = 10;
				this->label1->Top = 25;
				
				this->label2->Text = L"x steps number:";
				this->label2->Left = 10;
				this->label2->Top = 75;
				
				this->label3->Text = L"y step size:";
				this->label3->Left = 120;
				this->label3->Top = 25;
				
				this->label4->Text = L"y steps number:";
				this->label4->Left = 120;
				this->label4->Top = 75;
				
				this->numericUpDown1->Left = 10;
				this->numericUpDown1->Top = 40;
				this->numericUpDown1->Maximum = 1000;
				this->numericUpDown1->Minimum = 1;
				this->numericUpDown1->Value = 1;
				
				this->numericUpDown2->Left = 10;
				this->numericUpDown2->Top = 90;
				this->numericUpDown2->Maximum = 1000;
				this->numericUpDown2->Minimum = 1;
				this->numericUpDown2->Value = 1;
				
				this->numericUpDown3->Left = 120;
				this->numericUpDown3->Top = 40;
				this->numericUpDown3->Maximum = 1000;
				this->numericUpDown3->Minimum = 1;
				this->numericUpDown3->Value = 1;
				
				this->numericUpDown4->Left = 120;
				this->numericUpDown4->Top = 90;
				this->numericUpDown4->Maximum = 1000;
				this->numericUpDown4->Minimum = 1;
				this->numericUpDown4->Value = 1;
				
				this->button1->Text = L"Start";
				this->button2->Text = L"Stop";
				this->button3->Font = gcnew System::Drawing::Font(this->button3->Font->Name, this->button3->Font->Size - 2.8F, this->button3->Font->Style, this->button3->Font->Unit);
				this->button3->Text = L"Save background";
				this->button4->Text = L"Exit";
				this->button5->Text = L"Camera";
				this->button6->Text = L"Save Frame";

				this->button1->Enabled = false;
				this->button2->Enabled = false;
				this->button3->Enabled = false;
				
				this->button1->TextAlign = ContentAlignment::MiddleCenter;
				this->button2->TextAlign = ContentAlignment::MiddleCenter;
				this->button3->TextAlign = ContentAlignment::MiddleCenter;
				this->button4->TextAlign = ContentAlignment::MiddleCenter;
				this->button5->TextAlign = ContentAlignment::MiddleCenter;

				this->button1->Top = this->pictureBox1->Top + this->pictureBox1->Height + 15;
				this->button2->Top = this->button1->Top;
				this->button3->Top = this->button1->Top;
				this->button4->Top = this->button1->Top;
				this->button5->Top = this->button1->Top;
				this->button6->Top = this->button1->Top;

				this->button1->Width = 80;
				this->button2->Width = 80;
				this->button3->Width = 80;
				this->button4->Width = 80;
				this->button5->Width = 80;
				this->button6->Width = 80;

				this->button1->Height = 30;
				this->button2->Height = 30;
				this->button3->Height = 30;
				this->button4->Height = 30;
				this->button5->Height = 30;
				this->button6->Height = 30;

				this->button5->Left = 20;
				this->button6->Left = this->button5->Left + this->button5->Width + 20;
				this->button1->Left = this->button6->Left + this->button6->Width + 20;
				this->button2->Left = this->button1->Left + this->button1->Width + 20;
				this->button3->Left = this->button2->Left + this->button2->Width + 20;
				this->button4->Left = this->button3->Left + this->button3->Width + 20;

				DWORD minExp = 0;
				DWORD maxExp = 0;

				sdu_get_min_exp(Camera, &minExp);
				sdu_get_max_exp(Camera, &maxExp);
				
				this->label6->Text = L"Exposition:\n"
					+ L"Minimal " + System::Convert::ToString(static_cast<int>(minExp)) 
					+ L"\nMaximal " + System::Convert::ToString(static_cast<int>(maxExp));

				this->label6->Left = this->groupBox1->Left;
				this->label6->Top = this->groupBox1->Bottom + 12;

				this->numericUpDown5->Left = this->label6->Left;
				this->numericUpDown5->Top = this->label6->Bottom + 5;
				this->numericUpDown5->Maximum = System::Convert::ToDecimal(static_cast<int>(maxExp));
				this->numericUpDown5->Minimum = System::Convert::ToDecimal(static_cast<int>(minExp));
				this->numericUpDown5->Value = 1000;

				this->label14->Left = this->numericUpDown5->Right + 60;
				this->label14->Top = this->label6->Top;
				this->label14->Text = L"Amplification:\n"
					+ L"Values greater than " + System::Convert::ToString(255)  + "\n"
					+ L"are not considered";

				this->numericUpDown8->Left = this->numericUpDown5->Right + 60;
				this->numericUpDown8->Top = this->numericUpDown5->Top;
				this->numericUpDown8->Height = this->numericUpDown5->Height;
				this->numericUpDown8->Width = this->numericUpDown5->Width;
				this->numericUpDown8->Maximum = System::Convert::ToDecimal(255);
				this->numericUpDown8->Minimum = System::Convert::ToDecimal(1);
				this->numericUpDown8->Value = System::Convert::ToDecimal(1);
				this->numericUpDown8->DecimalPlaces = 1;
				this->numericUpDown8->Increment = System::Convert::ToDecimal(0.1);
				this->max_mult_allowed = 1.0;
				this->mult_set_by_user = 1.0;
				
				this->groupBox7->Left =this->groupBox1->Left;
				this->groupBox7->Width = ((this->Width - this->groupBox7->Left) > 300) ? 300 : (this->Width -this->groupBox7->Left - 10);
				this->groupBox7->Top = this->numericUpDown5->Bottom + 15;
				this->groupBox7->Height = ((this->groupBox7->Top + 220 + 15 + 195) < this->Height) ? 220 : (this->Height - this->groupBox7->Top - 230) ;
				this->groupBox7->Text = L"Manual control";
				
				this->button8->Width = 65;
				this->button9->Width = 65;
				this->button10->Width = 65;
				this->button11->Width = 65;
				this->button12->Width = 24;
				this->button14->Width = 24;
				this->button16->Width = 70;
				this->button8->Height = 65;
				this->button9->Height = 65;
				this->button10->Height = 65;
				this->button11->Height = 65;
				this->button12->Height = 24;
				this->button14->Height = 24;
				this->button16->Height = 30;
				float currentSize = this->button8->Font->Size;
				currentSize += 10.0F;
				this->button8->Font = gcnew System::Drawing::Font(this->button8->Font->Name, currentSize,
					this->button8->Font->Style, this->button8->Font->Unit);
				this->button9->Font = gcnew System::Drawing::Font(this->button9->Font->Name, currentSize,
					this->button9->Font->Style, this->button9->Font->Unit);
				this->button10->Font = gcnew System::Drawing::Font(this->button10->Font->Name, currentSize,
					this->button10->Font->Style, this->button10->Font->Unit);
				this->button11->Font = gcnew System::Drawing::Font(this->button11->Font->Name, currentSize,
					this->button11->Font->Style, this->button11->Font->Unit);

				this->button8->Text = L"←";
				this->button9->Text = L"↑";
				this->button10->Text = L"→";
				this->button11->Text = L"↓";
				this->button16->Text = L"Stop";
				
				this->button9->Top = 25;
				this->button8->Left = 15;
				this->button9->Left = this->button8->Right;
				this->button8->Top = this->button9->Top + this->button9->Height / 2;
				this->button10->Top = this->button8->Top;
				this->button10->Left = this->button9->Right;
				this->button11->Left = this->button9->Left;
				this->button11->Top = this->button9->Bottom;
				this->button16->Left = 15;
				this->button16->Top = this->groupBox7->Height - this->button16->Height - 10;
				this->label9->Left = this->groupBox7->Width - 65;
				this->label9->Top = 10;
				this->label9->Text = L"x step size:";
				
				this->numericUpDown6->Left = this->label9->Left;
				this->numericUpDown6->Width = this->label9->Width;
				this->numericUpDown6->Top = this->label9->Bottom + 3;
				this->numericUpDown6->Maximum = 20;
				this->numericUpDown6->Minimum = 2;
				this->numericUpDown6->Value = 2;

				this->label10->Left = this->label9->Left;
				this->label10->Top = this->numericUpDown6->Bottom + 5;
				this->label10->Text = L"y step size:";

				this->numericUpDown7->Left = this->label10->Left;
				this->numericUpDown7->Width = this->numericUpDown6->Width;
				this->numericUpDown7->Height = this->numericUpDown6->Height;
				this->numericUpDown7->Top = this->label10->Bottom + 3;
				this->numericUpDown7->Maximum = 20;
				this->numericUpDown7->Minimum = 2;
				this->numericUpDown7->Value = 2;
				
				this->label12->Left = this->groupBox7->Width / 3 * 2 - 40;
				this->label12->Top = this->groupBox7->Height - 25;
				this->label12->Text = L"Set the origin:";
				this->button12->Left = this->label12->Right + 4;
				this->button12->Top = this->label12->Top - 5;
				this->button12->Text = L"X";
				this->button14->Left = this->button12->Right + 2;
				this->button14->Top = this->button12->Top;
				this->button14->Text = L"Y";

				this->groupBox2->Text = L"Calibration settings";
				this->groupBox2->Left = this->groupBox1->Left;
				this->groupBox2->Width = ((this->Width - this->groupBox2->Left) > 300) ? 300 : (this->Width - this->groupBox2->Left - 10);
				this->groupBox2->Top = this->groupBox7->Bottom + 15;
				this->groupBox2->Height = ((this->Height - 10 - this->groupBox2->Top) > 185)? 185 : (this->button1->Height- 10 - this->groupBox2->Top);

				this->label7->Top = 21;
				this->label7->Left = 7;
				this->label7->Text = L"The first driver";
				
				this->button7->Top = 13;
				this->button7->Left = this->label7->Left + this->label7->Width + 30;
				this->button7->Height = 30;
				this->button7->Width = 80;
				this->button7->Text = L"CW rotation";

				this->groupBox3->Left = 7;
				this->groupBox3->Top = this->button7->Bottom + 5;
				this->groupBox3->Text = L"Direction";
				this->groupBox3->Width = 150;
				this->groupBox3->Height = 40;

				this->radioButton1->Text = L"Horizontal";
				this->radioButton2->Text = L"Vertical";
				this->radioButton2->Left = this->radioButton1->Right + 4;
				this->radioButton2->Top = this->radioButton1->Top;
				this->radioButton1->Checked = true;
				this->radioButton2->Checked = false;
				this->radioButton1->Enabled = false;
				this->radioButton2->Enabled = false;

				this->groupBox4->Width = 115;
				this->groupBox4->Height = this->groupBox3->Height;
				this->groupBox4->Text = L"";
				this->groupBox4->Top = this->groupBox3->Top;
				this->groupBox4->Left = this->groupBox3->Right + 15;

				this->radioButton3->Text = L"Rigth";
				this->radioButton4->Text = L"Left";
				this->radioButton3->Top = this->radioButton1->Top;
				this->radioButton4->Top = this->radioButton3->Top;
				this->radioButton3->Left = 3;
				this->radioButton4->Left = this->radioButton3->Right + 4;
				this->radioButton3->Enabled = false;
				this->radioButton4->Enabled = false;

				this->label8->Left = this->label7->Left;
				this->label8->Top = this->groupBox3->Bottom + 15;
				this->label8->Text = L"The second driver";

				this->button13->Left = this->button7->Left;
				this->button13->Width = this->button7->Width;
				this->button13->Height = this->button7->Height;
				this->button13->Top = this->groupBox3->Bottom + 7;
				this->button13->Text = L"CW rotation";
				this->button13->Enabled = false;

				
				this->groupBox5->Left = this->groupBox3->Left;
				this->groupBox6->Left = this->groupBox4->Left;
				this->groupBox5->Top = this->button13->Bottom + 5;
				this->groupBox6->Top = this->groupBox5->Top;
				this->groupBox5->Width = this->groupBox3->Width;
				this->groupBox6->Width = this->groupBox4->Width;
				this->groupBox5->Height = this->groupBox3->Height;
				this->groupBox6->Height = this->groupBox5->Height;
				this->groupBox5->Text = L"Direction";
				this->groupBox6->Text = L"";

				this->radioButton5->Left = this->radioButton1->Left;
				this->radioButton6->Left = this->radioButton1->Right + 4;
				this->radioButton5->Top = this->radioButton1->Top;
				this->radioButton6->Top = this->radioButton5->Top;
				this->radioButton5->Text = L"Horizontal";
				this->radioButton6->Text = L"Vertical";
				this->radioButton5->Checked = false;
				this->radioButton6->Checked = true;
				this->radioButton5->Enabled = false;
				this->radioButton6->Enabled = false;

				this->radioButton7->Left = this->radioButton3->Left;
				this->radioButton8->Left = this->radioButton4->Left;
				this->radioButton7->Top = this->radioButton3->Top;
				this->radioButton8->Top = this->radioButton4->Top;
				this->radioButton7->Text = L"Up";
				this->radioButton8->Text = L"Down";
				this->radioButton7->Enabled = false;
				this->radioButton8->Enabled = false;

				this->pixels = new unsigned char [this->pictureBox1->Width * this->pictureBox1->Height];
				this->background = new unsigned char [static_cast<int>(this->_roi_size_h * this->_roi_size_v)];

				for (int ii = 0; ii < static_cast<int>(this->_roi_size_h); ii++)
				{
					for (int jj = 0; jj < static_cast<int>(this->_roi_size_v); jj++)
					{
						this->background[ii * static_cast<int>(this->_roi_size_v) + jj] = 0.0;
					}
				}
				
				this->_exp = 1000;
				this->_period = 60000;
				this->_camStart = false;
				this->xScanWindows = 1;
				this->xStepsManual = 2;
				this->xStepsScanning = 1;
				this->xstepsDone = 0;
				this->yScanWindows = 1;
				this->yStepsManual = 2;
				this->yStepsScanning = 1;
				this->ystepsDone = 0;

				this->button15->Top = 5;
				this->button15->Height = 30;
				this->button15->Width = 80;
				this->button15->Text = L"Change";
				this->button15->Left = this->Right - 120;

				this->textBox2->Top = 11;
				this->textBox2->Width = 140;
				this->textBox2->Left = this->button15->Left - this->textBox2->Width - 15;
				this->textBox2->ReadOnly = true;
				this->textBox2->Multiline = false;

				this->textBox1->Top = 11;
				this->textBox1->Width = 120;
				this->textBox1->Left = this->textBox2->Left - this->textBox1->Width - 15;
				this->textBox1->Text = L"";
				this->label13->Top = 12;
				this->label13->Text = L"Base of the file name:";
				this->label13->Left = this->textBox1->Left - this->label13->Width - 15;

				/*** Driver Related Initialization ***/
				this->sPortName = L"COM4";
				this->hSerial = ::CreateFile(sPortName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			}
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MinimizeBox = false;
			this->MaximizeBox = false;

			DCB dcbSerialParams = {};
			dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
			//this->isCOMOpened = true;

			if (hSerial == INVALID_HANDLE_VALUE)	
			{
				if (GetLastError() == ERROR_FILE_NOT_FOUND)
				{
					 this->SetLabel5Text(L"COM port doesn't exist");
				}
				//this->SetLabel5Text(L"Some error occured");
				//this->isCOMOpened = false;
			}
			else
			{
				this->SetLabel5Text(L"COM port is opened");
				if (!GetCommState(hSerial, &dcbSerialParams))
				{
					this->SetLabel5Text(L"Error while getting state");
					//this->isCOMOpened = false;
				}
				dcbSerialParams.BaudRate = CBR_9600;
				dcbSerialParams.ByteSize = 8;
				dcbSerialParams.StopBits = ONESTOPBIT;
				dcbSerialParams.Parity = NOPARITY;
				if (!SetCommState(hSerial, &dcbSerialParams))
				{
					this->SetLabel5Text(L"Error while setting state");
					//this->isCOMOpened = false;
				}
			}
			this->KeyPreview = true;
			this->folderName = System::IO::Path::GetDirectoryName(System::Reflection::Assembly::GetExecutingAssembly()->GetName()->CodeBase);
			this->folderBrowserDialog1->SelectedPath = this->folderName;
			this->textBox2->Text = this->folderName->Substring(this->folderName->IndexOf("\\") + 1);
			this->folderName = this->folderName->Substring(this->folderName->IndexOf("\\") + 1) + L"\\" +  DateTime::Now.ToString("yyyyMMdd");
			this->_isFolderCreated = false;
			this->fileNameBase = L"scan_";
			this->textBox1->Text = this->fileNameBase;
			this->textBox1->ReadOnly = true;
			this->storedImageCounterLarge = 0;
			this->storedImageCounterSmall = 0;
			this->xstepsDone = 0;
			this->ystepsDone = 0;
			this->stepsInfoUpdate();

			char firstDriverSetFixedStepRegime[] = {':', '0', '1', 'S', 'E', '1', 0x0D};
			char secondDriverSetFixedStepRegime[] = {':', '0', '2', 'S', 'E', '1', 0x0D};
			char driverAnswer[7];
			BOOL iRet;
			DWORD iSize;

			iRet = WriteFile(hSerial, firstDriverSetFixedStepRegime, sizeof(firstDriverSetFixedStepRegime), &iSize, NULL);
			Sleep(100);
			iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);

			String^ stringToDisplay = gcnew String(driverAnswer);
			this->SetLabel5Text(stringToDisplay);

			iRet = WriteFile(hSerial, secondDriverSetFixedStepRegime, sizeof(secondDriverSetFixedStepRegime), &iSize, NULL);
			Sleep(100);
			iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			stringToDisplay = gcnew String(driverAnswer);
			this->cutOff = 0.65;
			this->max_mult_allowed = 1.0';
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if ((this->trd) && (this->trd->IsAlive))
			{
				this->trd->Abort();
			}

			if ((this->camTrd) && (this->camTrd->IsAlive))
			{
				this->camTrd->Abort();
			}

			sdu_close_interface(this->Camera);
			if (this->data)
			{
				delete [] this->data;
			}
			
			if (this->tdata)
			{
				delete [] this->tdata;
			}
			
			if (this->pixels)
			{
				delete [] this->pixels;
			}

			if (this->dataToPrint)
			{
				delete [] this->dataToPrint;
			}

			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected: 
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown4;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown3;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  button4;

	private:
		/// <summary>
		/// Required designer variable.
		volatile bool _camStatus;    //Current status of the camera: whether it is working or staying
		volatile bool _camStart;
		volatile bool _isCalibrated; //the flag to check whether the settings are chosen
		volatile bool _isStarted; // the flag to enable "Stop" and "Save Image" buttons
		volatile bool _isDataCanBeRead;
		volatile bool _isFolderCreated;
		bool _isImageStored;
		bool _isCOMInitialized;
		bool _isXOriginChanged;
		bool _isYOriginChanged;
		
		int _exp;
		volatile int _processLaunched; //the flag of the current process (to be decided)
		int _width;
		int _height;
		int _picWidth;
		int _picHeight;
		int _period;
		int _direction; //the image processing can be along	x and y directions
		int _minPeriod;
		DWORD _roi_size_h;
		DWORD _roi_size_v;
		unsigned char  *data;
		unsigned char *tdata; //data received from the camera
		unsigned char *pixels; //data to be shown on the screen
		unsigned char *background;
		unsigned char *dataToPrint; //Full image to be stored as a bitmap file
		double cutOff;
		double max_mult_allowed; //for artificial amplification purposes
		double mult_set_by_user; //value set in numericUpDown
		CSDUCameraDevice *Camera;
		int xStepsScanning; //I suppose, that it is a number of steps to be done in each direction
		int yStepsScanning;
		int xScanWindows;
		int yScanWindows;
		int xstepsDone; //rotation of camera already done
		int ystepsDone;
		HANDLE hSerial;
		LPCTSTR sPortName;
		char xdriver;
		char ydriver;
		char xleft;
		char xright;
		char yup;
		char ydown;
		int xStepsManual;
		int yStepsManual;
		int storedImageCounterSmall;
		int storedImageCounterLarge;
		String^ folderName;
		String^ fileNameBase;
		String^ currentOutFileName;

private: Thread ^trd;
private: Thread ^camTrd;

private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::Button^  button5;
private: System::Windows::Forms::NumericUpDown^  numericUpDown5;
private: System::Windows::Forms::Button^  button6;
private: System::Windows::Forms::GroupBox^  groupBox2;
private: System::Windows::Forms::GroupBox^  groupBox3;
private: System::Windows::Forms::Button^  button7;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::RadioButton^  radioButton2;
private: System::Windows::Forms::RadioButton^  radioButton1;
private: System::Windows::Forms::GroupBox^  groupBox4;
private: System::Windows::Forms::RadioButton^  radioButton3;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::RadioButton^  radioButton4;
private: System::Windows::Forms::GroupBox^  groupBox6;
private: System::Windows::Forms::GroupBox^  groupBox5;
private: System::Windows::Forms::RadioButton^  radioButton5;
private: System::Windows::Forms::RadioButton^  radioButton8;
private: System::Windows::Forms::RadioButton^  radioButton7;
private: System::Windows::Forms::RadioButton^  radioButton6;
private: System::Windows::Forms::NumericUpDown^  numericUpDown7;
private: System::Windows::Forms::NumericUpDown^  numericUpDown6;
private: System::Windows::Forms::Button^  button12;
private: System::Windows::Forms::Button^  button11;
private: System::Windows::Forms::Button^  button10;
private: System::Windows::Forms::Button^  button9;
private: System::Windows::Forms::Button^  button8;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Button^  button13;
private: System::Windows::Forms::Label^  label12;
private: System::Windows::Forms::Label^  label11;
private: System::Windows::Forms::Button^  button14;
private: System::Windows::Forms::GroupBox^  groupBox7;
private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
private: System::Windows::Forms::Button^  button15;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::Label^  label13;
private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::Button^  button16;
private: System::Windows::Forms::NumericUpDown^  numericUpDown8;
private: System::Windows::Forms::Label^  label14;


		 /// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown4 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->numericUpDown5 = (gcnew System::Windows::Forms::NumericUpDown());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton8 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton7 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown7 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown6 = (gcnew System::Windows::Forms::NumericUpDown());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button16 = (gcnew System::Windows::Forms::Button());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->numericUpDown8 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label14 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown5))->BeginInit();
			this->groupBox2->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown6))->BeginInit();
			this->groupBox7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown8))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(24, 32);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(291, 242);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(90, 469);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(73, 26);
			this->button1->TabIndex = 1;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(189, 469);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(73, 26);
			this->button2->TabIndex = 2;
			this->button2->Text = L"button2";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(291, 469);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(73, 26);
			this->button3->TabIndex = 3;
			this->button3->Text = L"button3";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->numericUpDown4);
			this->groupBox1->Controls->Add(this->numericUpDown3);
			this->groupBox1->Controls->Add(this->numericUpDown2);
			this->groupBox1->Controls->Add(this->numericUpDown1);
			this->groupBox1->Location = System::Drawing::Point(334, 32);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(222, 128);
			this->groupBox1->TabIndex = 4;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"groupBox1";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(130, 63);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(35, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"label4";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(130, 21);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"label3";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 63);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(35, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"label2";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 21);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"label1";
			// 
			// numericUpDown4
			// 
			this->numericUpDown4->Location = System::Drawing::Point(112, 91);
			this->numericUpDown4->Name = L"numericUpDown4";
			this->numericUpDown4->Size = System::Drawing::Size(71, 20);
			this->numericUpDown4->TabIndex = 3;
			this->numericUpDown4->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown4_ValueChanged);
			// 
			// numericUpDown3
			// 
			this->numericUpDown3->Location = System::Drawing::Point(112, 40);
			this->numericUpDown3->Name = L"numericUpDown3";
			this->numericUpDown3->Size = System::Drawing::Size(71, 20);
			this->numericUpDown3->TabIndex = 2;
			this->numericUpDown3->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown3_ValueChanged);
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->Location = System::Drawing::Point(6, 91);
			this->numericUpDown2->Name = L"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(71, 20);
			this->numericUpDown2->TabIndex = 1;
			this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown2_ValueChanged);
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->Location = System::Drawing::Point(6, 40);
			this->numericUpDown1->Name = L"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(71, 20);
			this->numericUpDown1->TabIndex = 0;
			this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown1_ValueChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(37, 9);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 13);
			this->label5->TabIndex = 5;
			this->label5->Text = L"label5";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(389, 469);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(73, 26);
			this->button4->TabIndex = 6;
			this->button4->Text = L"button4";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(337, 163);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(35, 13);
			this->label6->TabIndex = 7;
			this->label6->Text = L"label6";
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(11, 469);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(73, 26);
			this->button5->TabIndex = 8;
			this->button5->Text = L"button5";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// numericUpDown5
			// 
			this->numericUpDown5->Location = System::Drawing::Point(334, 179);
			this->numericUpDown5->Name = L"numericUpDown5";
			this->numericUpDown5->Size = System::Drawing::Size(71, 20);
			this->numericUpDown5->TabIndex = 8;
			this->numericUpDown5->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown5_ValueChanged);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(490, 468);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(71, 26);
			this->button6->TabIndex = 9;
			this->button6->Text = L"button6";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button13);
			this->groupBox2->Controls->Add(this->groupBox6);
			this->groupBox2->Controls->Add(this->groupBox5);
			this->groupBox2->Controls->Add(this->label8);
			this->groupBox2->Controls->Add(this->groupBox4);
			this->groupBox2->Controls->Add(this->groupBox3);
			this->groupBox2->Controls->Add(this->button7);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Location = System::Drawing::Point(341, 218);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(292, 205);
			this->groupBox2->TabIndex = 10;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"groupBox2";
			// 
			// button13
			// 
			this->button13->Location = System::Drawing::Point(225, 139);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(60, 31);
			this->button13->TabIndex = 16;
			this->button13->Text = L"button13";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &Form1::button13_Click);
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->radioButton8);
			this->groupBox6->Controls->Add(this->radioButton7);
			this->groupBox6->Location = System::Drawing::Point(122, 132);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(97, 67);
			this->groupBox6->TabIndex = 6;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"groupBox6";
			// 
			// radioButton8
			// 
			this->radioButton8->AutoSize = true;
			this->radioButton8->Location = System::Drawing::Point(4, 44);
			this->radioButton8->Name = L"radioButton8";
			this->radioButton8->Size = System::Drawing::Size(85, 17);
			this->radioButton8->TabIndex = 1;
			this->radioButton8->TabStop = true;
			this->radioButton8->Text = L"radioButton8";
			this->radioButton8->UseVisualStyleBackColor = true;
			this->radioButton8->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton8_CheckedChanged);
			// 
			// radioButton7
			// 
			this->radioButton7->AutoSize = true;
			this->radioButton7->Location = System::Drawing::Point(4, 21);
			this->radioButton7->Name = L"radioButton7";
			this->radioButton7->Size = System::Drawing::Size(85, 17);
			this->radioButton7->TabIndex = 0;
			this->radioButton7->TabStop = true;
			this->radioButton7->Text = L"radioButton7";
			this->radioButton7->UseVisualStyleBackColor = true;
			this->radioButton7->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton7_CheckedChanged);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->radioButton6);
			this->groupBox5->Controls->Add(this->radioButton5);
			this->groupBox5->Location = System::Drawing::Point(8, 132);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(100, 68);
			this->groupBox5->TabIndex = 5;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"groupBox5";
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Location = System::Drawing::Point(9, 44);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(85, 17);
			this->radioButton6->TabIndex = 1;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"radioButton6";
			this->radioButton6->UseVisualStyleBackColor = true;
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(11, 21);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(85, 17);
			this->radioButton5->TabIndex = 0;
			this->radioButton5->TabStop = true;
			this->radioButton5->Text = L"radioButton5";
			this->radioButton5->UseVisualStyleBackColor = true;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(15, 116);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(35, 13);
			this->label8->TabIndex = 4;
			this->label8->Text = L"label8";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->radioButton4);
			this->groupBox4->Controls->Add(this->radioButton3);
			this->groupBox4->Location = System::Drawing::Point(122, 43);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(95, 61);
			this->groupBox4->TabIndex = 3;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"groupBox4";
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Location = System::Drawing::Point(5, 36);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(85, 17);
			this->radioButton4->TabIndex = 1;
			this->radioButton4->TabStop = true;
			this->radioButton4->Text = L"radioButton4";
			this->radioButton4->UseVisualStyleBackColor = true;
			this->radioButton4->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton4_CheckedChanged);
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(4, 19);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(85, 17);
			this->radioButton3->TabIndex = 0;
			this->radioButton3->TabStop = true;
			this->radioButton3->Text = L"radioButton3";
			this->radioButton3->UseVisualStyleBackColor = true;
			this->radioButton3->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton3_CheckedChanged);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->radioButton2);
			this->groupBox3->Controls->Add(this->radioButton1);
			this->groupBox3->Location = System::Drawing::Point(8, 43);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(101, 62);
			this->groupBox3->TabIndex = 2;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"groupBox3";
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(6, 39);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(85, 17);
			this->radioButton2->TabIndex = 1;
			this->radioButton2->TabStop = true;
			this->radioButton2->Text = L"radioButton2";
			this->radioButton2->UseVisualStyleBackColor = true;
			this->radioButton2->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton2_CheckedChanged);
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Location = System::Drawing::Point(6, 16);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(85, 17);
			this->radioButton1->TabIndex = 0;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"radioButton1";
			this->radioButton1->UseVisualStyleBackColor = true;
			this->radioButton1->CheckedChanged += gcnew System::EventHandler(this, &Form1::radioButton1_CheckedChanged);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(59, 16);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(99, 21);
			this->button7->TabIndex = 1;
			this->button7->Text = L"button7";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(16, 20);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(35, 13);
			this->label7->TabIndex = 0;
			this->label7->Text = L"label7";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(175, 74);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(41, 13);
			this->label12->TabIndex = 19;
			this->label12->Text = L"label12";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(160, 9);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(41, 13);
			this->label11->TabIndex = 18;
			this->label11->Text = L"label11";
			// 
			// button14
			// 
			this->button14->Location = System::Drawing::Point(216, 104);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(65, 24);
			this->button14->TabIndex = 17;
			this->button14->Text = L"button14";
			this->button14->UseVisualStyleBackColor = true;
			this->button14->Click += gcnew System::EventHandler(this, &Form1::button14_Click);
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(95, 29);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(41, 13);
			this->label10->TabIndex = 15;
			this->label10->Text = L"label10";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(175, 61);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(35, 13);
			this->label9->TabIndex = 14;
			this->label9->Text = L"label9";
			// 
			// numericUpDown7
			// 
			this->numericUpDown7->Location = System::Drawing::Point(173, 22);
			this->numericUpDown7->Name = L"numericUpDown7";
			this->numericUpDown7->Size = System::Drawing::Size(37, 20);
			this->numericUpDown7->TabIndex = 13;
			this->numericUpDown7->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown7_ValueChanged);
			// 
			// numericUpDown6
			// 
			this->numericUpDown6->Location = System::Drawing::Point(99, 52);
			this->numericUpDown6->Name = L"numericUpDown6";
			this->numericUpDown6->Size = System::Drawing::Size(37, 20);
			this->numericUpDown6->TabIndex = 12;
			this->numericUpDown6->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown6_ValueChanged);
			// 
			// button12
			// 
			this->button12->Location = System::Drawing::Point(171, 99);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(39, 29);
			this->button12->TabIndex = 11;
			this->button12->Text = L"button12";
			this->button12->UseVisualStyleBackColor = true;
			this->button12->Click += gcnew System::EventHandler(this, &Form1::button12_Click);
			// 
			// button11
			// 
			this->button11->Location = System::Drawing::Point(18, 89);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(39, 29);
			this->button11->TabIndex = 10;
			this->button11->Text = L"button11";
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &Form1::button11_Click);
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(81, 99);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(39, 29);
			this->button10->TabIndex = 9;
			this->button10->Text = L"button10";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(126, 99);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(39, 29);
			this->button9->TabIndex = 8;
			this->button9->Text = L"button9";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(242, 66);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(39, 29);
			this->button8->TabIndex = 7;
			this->button8->Text = L"button8";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &Form1::button8_Click);
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button16);
			this->groupBox7->Controls->Add(this->label12);
			this->groupBox7->Controls->Add(this->button14);
			this->groupBox7->Controls->Add(this->button12);
			this->groupBox7->Controls->Add(this->button9);
			this->groupBox7->Controls->Add(this->label9);
			this->groupBox7->Controls->Add(this->label10);
			this->groupBox7->Controls->Add(this->numericUpDown6);
			this->groupBox7->Controls->Add(this->button10);
			this->groupBox7->Controls->Add(this->button11);
			this->groupBox7->Controls->Add(this->numericUpDown7);
			this->groupBox7->Controls->Add(this->button8);
			this->groupBox7->Location = System::Drawing::Point(27, 305);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(287, 134);
			this->groupBox7->TabIndex = 11;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"groupBox7";
			// 
			// button16
			// 
			this->button16->Location = System::Drawing::Point(11, 44);
			this->button16->Name = L"button16";
			this->button16->Size = System::Drawing::Size(52, 29);
			this->button16->TabIndex = 20;
			this->button16->Text = L"button16";
			this->button16->UseVisualStyleBackColor = true;
			this->button16->Click += gcnew System::EventHandler(this, &Form1::button16_Click);
			// 
			// button15
			// 
			this->button15->Location = System::Drawing::Point(591, 9);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(42, 31);
			this->button15->TabIndex = 19;
			this->button15->Text = L"button15";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &Form1::button15_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(362, 8);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(65, 20);
			this->textBox1->TabIndex = 20;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &Form1::textBox1_TextChanged);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(280, 9);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(41, 13);
			this->label13->TabIndex = 21;
			this->label13->Text = L"label13";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(465, 13);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(86, 20);
			this->textBox2->TabIndex = 22;
			// 
			// numericUpDown8
			// 
			this->numericUpDown8->Location = System::Drawing::Point(428, 179);
			this->numericUpDown8->Name = L"numericUpDown8";
			this->numericUpDown8->Size = System::Drawing::Size(71, 20);
			this->numericUpDown8->TabIndex = 23;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(435, 163);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(41, 13);
			this->label14->TabIndex = 24;
			this->label14->Text = L"label14";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(645, 520);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->numericUpDown8);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button15);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->numericUpDown5);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Form1";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->PreviewKeyDown += gcnew System::Windows::Forms::PreviewKeyDownEventHandler(this, &Form1::Form1_PreviewKeyDown);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown5))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown6))->EndInit();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->numericUpDown8))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
			 }

delegate void DelegateSetLabel5Text(System::String^ text);
private: void SetLabel5Text(System::String^ text)
		 {
			 if (this->label5->InvokeRequired)
			 {
				 DelegateSetLabel5Text^ d = gcnew DelegateSetLabel5Text(this, &Form1::SetLabel5Text);
				 this->Invoke(d, gcnew array<Object^>{text});
			 }
			 else
			 {
				 this->label5->Text = text;
			 }
		 }

delegate void DelegateSetLabel11Text(System::String^ text);
private: void SetLabel11Text(System::String^ text)
		 {
			 if (this->label11->InvokeRequired)
			 {
				 DelegateSetLabel11Text^ d = gcnew DelegateSetLabel11Text(this, &Form1::SetLabel11Text);
				 this->Invoke(d, gcnew array<Object^>{text});
			 }
			 else
			 {
				 this->label11->Text = text;
			 }
		 }

private: void GetAndDrawPicture(Bitmap ^img)
		 {
			DWORD status = 0;
			DWORD real_length = 0;
			int i = 0;
			int j = 0;

			int rsh = static_cast<int>(this->_roi_size_h);
			int rsv = static_cast<int>(this->_roi_size_v);
			sdu_fifo_init(this->Camera);
			
			while (this->_camStart)
			{
				this->camTrd->Sleep(this->_period / 1000 );
				sdu_cam_start(this->Camera);
				while(true)
				{
					sdu_get_status(this->Camera, &status);

					if (!(status & SDU_STAT_BUSY) /*&& !(status & SDU_STAT_FIFOEMPTY)*/)
					{
						break;
					}
				}
									
				//if (!(status & SDU_STAT_FIFOEMPTY))
				{
					 //since then the data are no more valid
					this->_isDataCanBeRead = false;
					sdu_read_data(this->Camera, data, sizeof(unsigned char) * this->_roi_size_h * this->_roi_size_v, &real_length);

					for (i = 0; i < rsh; i++)
					{
						for(j = 0; j < rsv; j++)
						{
							tdata[i * rsv + j] = data[j * rsh + i];
						}
					}

					this->_isDataCanBeRead = true;
					resizeImageSimple(tdata, pixels, rsh, rsv, this->pictureBox1->Width, this->pictureBox1->Height);
					
					int x = 0;
					int y = 0;
						
					for (x = 0; x < img->Width; x++)
					{
						for(y = 0; y < img->Height; y++)
						{
							int c = (this->pixels[ x * this->pictureBox1->Height + y]);
							Color pixCol = Color::FromArgb(c, c, c);
							img->SetPixel(x, y, pixCol);
						}
					}
					System::Drawing::Rectangle cloneRect = System::Drawing::Rectangle(0, 0, pictureBox1->Width, pictureBox1->Height);
					System::Drawing::Imaging::PixelFormat format = img->PixelFormat;
					Bitmap^ cloneBitmap = img->Clone( cloneRect, format );
					pictureBox1->Image = cloneBitmap;//img;
				}
			}	
			sdu_cam_stop(this->Camera);
		 }


private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (this->_camStart)
			 {
				 sdu_cam_stop(this->Camera);
				 this->_camStatus = false;
			 }
			 if ((this->camTrd) && (this->camTrd->IsAlive))
			 {
				 this->camTrd->Abort();
			 }
			 if ((this->trd) && (this->trd->IsAlive))
			 {
				 this->trd->Abort();
			 }
			 this->enableButtons();
		 }

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->disableButtons();
//TO BE TOTALLY REVISED
			 this->moveToTheScanningOrigin();

			 long int current_size = this->xScanWindows * this->yScanWindows * static_cast<long int>(this->_roi_size_h * this->_roi_size_v);
			 long int array_size = (!this->dataToPrint) ? 0 : (sizeof(this->dataToPrint) / sizeof(unsigned char));

			 if (current_size != array_size)
			 {
				 delete [] this->dataToPrint;
				 this->dataToPrint = new unsigned char [current_size];
				 this->_isImageStored = false;
			 }
			 this->setXSteps(this->xStepsScanning);
			 this->setYSteps(this->yStepsScanning);
			 
			// this->_camStatus = false;

			 //sdu_cam_stop(this->Camera);
			 
			 if ((this->trd) && (this->trd->IsAlive))
			 {
				 this->trd->Abort();
			 }
			 ThreadStart ^myThreadDelegate = gcnew ThreadStart(this, &Microscope_driver::Form1::ScanningTask);
			 trd = gcnew Thread(myThreadDelegate);
			 trd->IsBackground = true;
			 trd->Start();
		 }
delegate void DelegateScanningTask();
private: void ScanningTask()
			{
				//sdu_fifo_init(this->Camera);
				if (!this->_camStart)
				{
					this->button5->PerformClick();
				}

				int cx = 0;
				int cy = 0;
				int rsh = static_cast<int>(this->_roi_size_h);
				int rsv = static_cast<int>(this->_roi_size_v);
				int xw = this->xScanWindows;
				int yw = this->yScanWindows;

				Bitmap^ img = gcnew Bitmap(this->pictureBox1->Width, this->pictureBox1->Height);
				int time_to_wait = 11000; //(static_cast<int>(this->_period) > this->_exp) ? static_cast<int>((this->_period - this->_exp) / 1000 + 1) : 1;

				for (cx = 0; cx < xw; cx++)
				{
					for (cy = 0; cy < yw - 1; cy++)
					{
						//this->GetAndDrawPicture(img);
						for (int i = 0; i < rsh; i++)
						{
							for (int j = 0; j < rsv; j++)
							{
								this->dataToPrint[(cx * rsh + i) * rsv * yw + rsv * cy + j] = this->tdata[i * rsv + j];
							}
						}
						this->button6->PerformClick();
						this->trd->Sleep(time_to_wait);
						this->moveDown();
						this->trd->Sleep(time_to_wait);
					}
					//this->GetAndDrawPicture(img);
					for (int i = 0; i < rsh; i++)
					{
						for (int j = 0; j < rsv; j++)
						{
							this->dataToPrint[(cx * rsh + i) * rsv * yw + rsv * (yw - 1) + j] = this->tdata[i * rsv + j];
						}
					}
					this->button6->PerformClick();
					this->trd->Sleep(time_to_wait);
					cx++;
					this->moveRight();
					this->trd->Sleep(time_to_wait);
					if (cx < xw)
					{
						for (cy = yw - 1; cy >= 1; cy--)
						{
							//this->GetAndDrawPicture(img);
							for (int i = 0; i < static_cast<int>(this->_roi_size_h); i++)
							{	
								for (int j = 0; j < static_cast<int>(this->_roi_size_v); j++)
								{
									this->dataToPrint[(cx * rsh + i) * rsv * yw + rsv * cy + j] = this->tdata[i * rsv + j];
								}
							}
							this->button6->PerformClick();
							this->trd->Sleep(time_to_wait);
							this->moveUp();
							this->trd->Sleep(time_to_wait);
						}
						//this->GetAndDrawPicture(img);
						for (int i = 0; i < rsh; i++)
						{
							for (int j = 0; j < rsv; j++)
							{
								this->dataToPrint[(cx * rsh + i) * rsv * yw + j] = this->tdata[i * rsv + j];
							}
						}
						this->button6->PerformClick();
						this->trd->Sleep(time_to_wait);
						this->moveRight();
						this->trd->Sleep(time_to_wait);
					}
				}
				this->currentOutFileName = this->folderName + L"//frames//" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterLarge++)
				 + "_large.dat";
				this->storedImageCounterLarge++;
				this->saveToBITMAP(this->dataToPrint, yw * rsv, xw * rsh);
				this->enableButtons();
				this->trd->Abort();

			}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->button3->Enabled = true;
			 if (!this->_camStart)
			 {
				 this->_camStart = true;

				 if ((this->camTrd) && (this->camTrd->IsAlive))
				 {
					 this->camTrd->Abort();
				 }

				 ThreadStart ^myThreadDelegate = gcnew ThreadStart(this, &Microscope_driver::Form1::CameraTask);
				 this->camTrd = gcnew Thread(myThreadDelegate);
				 this->camTrd->IsBackground = true;
				 this->camTrd->Start();

			 }
			 /*if (this->_camStatus)
			 {
				 sdu_cam_stop(this->Camera);
			 }
			 this->_camStatus = false;
			 if ((this->trd) && (this->trd->IsAlive))
			 {
				 this->trd->Abort();
			 }
			 this->_camStatus = true;
			 ThreadStart ^myThreadDelegate = gcnew ThreadStart(this, &Microscope_driver::Form1::CameraTask);
			 trd = gcnew Thread(myThreadDelegate);
			 trd->IsBackground = true;
			 trd->Start();					*/
		 }

delegate void DelegateCameraTask();
private: void CameraTask()
		 {
			 Bitmap ^img = gcnew Bitmap(this->pictureBox1->Width, this->pictureBox1->Height);

			 sdu_fifo_init(this->Camera);
			 //int time_to_wait = (static_cast<int>(this->_period) > this->_exp) ? static_cast<int>((this->_period - this->_exp) / 1000 + 1) : 1;
			 int period = (2 * this->_exp > this->_minPeriod) ? (2 * this->_exp) : (this->_minPeriod);

			 //sdu_set_period(this->Camera, static_cast<DWORD>(period));
			 this->_camStart = true;

			// while(this->_camStatus)
			// {
				this->GetAndDrawPicture(img);	
				//this->trd->Sleep(time_to_wait);
			// }
		 }

private: System::Void numericUpDown5_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (this->_camStart)
			 {
				 this->_camStart = false;
				// sdu_cam_stop(this->Camera);
			 
				 if ((this->camTrd) && (this->camTrd->IsAlive))
				 {
					 this->camTrd->Abort();
				 }
				 this->_exp = static_cast<int>(System::Convert::ToInt32(this->numericUpDown5->Value));

				 sdu_set_exp(this->Camera, static_cast<DWORD>(this->_exp));		
				 this->_isImageStored = false;
				 this->button5->PerformClick();
			 }
			 else
			 {
				this->_exp = static_cast<int>(System::Convert::ToInt32(this->numericUpDown5->Value));
				sdu_set_exp(this->Camera, static_cast<DWORD>(this->_exp));		
			 }
			 
		 }
 private: void findImageIntersection()
		 {
		 }	
private: System::Void radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->radioButton3->Text = L"Right";
			 this->radioButton4->Text = L"Left";
			 this->radioButton5->Enabled = false;
			 this->radioButton6->Checked = true;
			 this->radioButton6->Enabled = false;
			 this->radioButton7->Text = L"Up";
			 this->radioButton8->Text = L"Down";
			 this->radioButton3->Checked = false;
			 this->radioButton4->Checked = false;
			 this->radioButton7->Checked = false;
			 this->radioButton8->Checked = false;
			 this->radioButton3->Enabled = false;
			 this->radioButton4->Enabled = false;
			 this->radioButton7->Enabled = false; 
			 this->radioButton8->Enabled = false;
			 this->button1->Enabled = false;
			 this->xdriver = '1';
			 this->ydriver = '2';
		 }
private: System::Void radioButton2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->radioButton3->Text = L"Up";
			 this->radioButton4->Text = L"Down";
			 this->radioButton5->Checked = true;
			 this->radioButton6->Checked = false;
			 this->radioButton5->Enabled = false;
			 this->radioButton6->Enabled = false;
			 this->radioButton7->Text = L"Right";
			 this->radioButton8->Text = L"Left";
			 this->radioButton3->Checked = false;
			 this->radioButton4->Checked = false;
			 this->radioButton7->Checked = false;
			 this->radioButton8->Checked = false;
			 this->radioButton3->Enabled = false;
			 this->radioButton4->Enabled = false;
			 this->radioButton7->Enabled = false;
			 this->radioButton8->Enabled = false;
			 this->button1->Enabled = false;
			 this->xdriver = '2';
			 this->ydriver = '1';
		 }
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->xstepsDone = 0;
			 this->stepsInfoUpdate();
		 }
private: System::Void button14_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->ystepsDone = 0;
			 this->stepsInfoUpdate();
		 }
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
			 char firstDriverFixedStepRegime[] = {':', '0', '1', 'S', 'E', 0x0D};
			 char firstDriverStepCount[] = {':', '0', '1', 'S', 'C', '6', 0x0D};
			 char firstDriverCWStep[] = {':', '0', '1', 'R', '+', 0x0D};
			 char driverAnswer[20];
			 DWORD iSize;
			 BOOL iRet;

			 this->radioButton1->Enabled = true;
			 this->radioButton2->Enabled = true;
			 this->radioButton3->Enabled = true;
			 this->radioButton4->Enabled = true;
			 this->button13->Enabled = true;


			 iRet = WriteFile(hSerial, firstDriverStepCount, sizeof(firstDriverStepCount), &iSize, NULL);
			 Sleep(500);
			 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);

			 iRet = WriteFile(hSerial, firstDriverCWStep, sizeof(firstDriverCWStep), &iSize, NULL);
			 Sleep(500);
			 iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);

			 this->setXSteps(this->xStepsManual);
			 this->setYSteps(this->yStepsManual);

		 }
private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
			 char secondDriverFixedStepRegime[] = {':', '0', '2', 'S', 'E', 0x0D};
			 char secondDriverStepCount[] = {':', '0', '2', 'S', 'C','6', 0x0D};
			 char secondDriverCWStep[] = {':', '0', '2', 'R', '+', 0x0D};
			 char driverAnswer[20];
			 DWORD iSize;
			 BOOL iRet;

			 this->radioButton7->Enabled = true;
			 this->radioButton8->Enabled = true;


			 iRet = WriteFile(hSerial, secondDriverStepCount, sizeof(secondDriverStepCount), &iSize, NULL);
			 Sleep(800);
			 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);

			 iRet = WriteFile(hSerial, secondDriverCWStep, sizeof(secondDriverCWStep), &iSize, NULL);
			 Sleep(800);
			 iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);

			 this->setXSteps(this->xStepsManual);
			 this->setYSteps(this->yStepsManual);
			 
		 }
private: System::Void radioButton3_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 bool secondDriverChecked = ((this->radioButton7->Checked) || (this->radioButton8->Checked));
			 if (secondDriverChecked)
			 {
				 this->button1->Enabled = true;
			 }
			 if (this->radioButton1->Checked)
			 {
				 this->xright = '+';
				 this->xleft = '-';
			 }
			 if (this->radioButton2->Checked)
			 {
				 this->yup = '+';
				 this->ydown = '-';
			 }
		 }
private: System::Void radioButton4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 bool secondDriverChecked = ((this->radioButton7->Checked) || (this->radioButton8->Checked));
			 if (secondDriverChecked)
			 {
				 this->button1->Enabled = true;
			 }
			 if (this->radioButton1->Checked)
			 {
				 this->xright = '-';
				 this->xleft = '+';
			 }
			 if (this->radioButton2->Checked)
			 {
				 this->yup = '-';
				 this->ydown = '+';
			 }
		 }
private: System::Void radioButton7_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 bool firstDriverChecked = ((this->radioButton3->Checked) || (this->radioButton4->Checked));
			 if (firstDriverChecked)
			 {
				 this->button1->Enabled = true;
			 }
			 if (this->radioButton1->Checked)
			 {
				 this->yup = '+';
				 this->ydown = '-';
			 }
			 if (this->radioButton2->Checked)
			 {
				 this->xright = '+';
				 this->xleft = '-';
			 }
		 }
private: System::Void radioButton8_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 bool firstDriverChecked = ((this->radioButton3->Checked) || (this->radioButton4->Checked));
			 if (firstDriverChecked)
			 {

				 this->button1->Enabled = true;
			 }
			 if (this->radioButton1->Checked)
			 {
				 this->yup = '-';
				 this->ydown = '+';
			 }
			 if (this->radioButton2->Checked)
			 {
				 this->xright = '-';
				 this->xleft = '+';
			 }
		 }
private: System::Void numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->xStepsScanning = static_cast<int>(System::Convert::ToInt32(this->numericUpDown1->Value));
		 }
private: System::Void numericUpDown2_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->xScanWindows = static_cast<int>(System::Convert::ToInt32(this->numericUpDown2->Value));
		 }
private: System::Void numericUpDown3_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->yStepsScanning = static_cast<int>(System::Convert::ToInt32(this->numericUpDown3->Value));
		 }
private: System::Void numericUpDown4_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->yScanWindows = static_cast<int>(System::Convert::ToInt32(this->numericUpDown4->Value));
		 }
private: System::Void numericUpDown6_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->xStepsManual = static_cast<int>(System::Convert::ToInt32(this->numericUpDown6->Value));

			 this->setXSteps(this->xStepsManual);
		 }
private: System::Void numericUpDown7_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->yStepsManual = static_cast<int>(System::Convert::ToInt32(this->numericUpDown7->Value));

			 this->setYSteps(this->yStepsManual);
			 
		 }
private: System::Void setXSteps(int steps)
		 {
			 BOOL iRet;
			 DWORD iSize;
			 char driverAnswer[7];
			 
			 if (steps < 0)
			 {
				 steps *= -1;
			 }

			 if (steps < 2)
			 {
				 steps = 2;
			 }

			 if (steps / 1000)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', '0', '0', 0x0D};

				 int d1 = steps / 1000;
				 int d2 = (steps - d1 * 1000) / 100;
				 int d3 = (steps - d1 * 1000 - d2 * 100) / 10;
				 int d4 = steps % 10;
				 
				 setStepsCount[2] = this->xdriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';
				 setStepsCount[7] = static_cast<char>(d3) + '0';
				 setStepsCount[8] = static_cast<char>(d4) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else if (steps / 100)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', '0', 0x0D};

				 int d1 = steps / 100;
				 int d2 = (steps - d1 * 100) / 10;
				 int d3 = steps % 10;
				 
				 setStepsCount[2] = this->xdriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';
				 setStepsCount[7] = static_cast<char>(d3) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else if (steps / 10)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', 0x0D};

				 int d1 = steps / 10;
				 int d2 = steps % 10;

				 setStepsCount[2] = this->xdriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', 0x0D};


				 setStepsCount[2] = this->xdriver;
				 setStepsCount[5] = static_cast<char>(steps) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 };
		 }
private: System::Void setYSteps(int steps)
		 {
			 BOOL iRet;
			 DWORD iSize;
			 char driverAnswer[7];
			 
			 if (steps < 0)
			 {
				 steps *= -1;
			 }

			 if (steps < 2)
			 {
				 steps = 2;
			 }

			 if (steps / 1000)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', '0', '0', 0x0D};

				 int d1 = steps / 1000;
				 int d2 = (steps - d1 * 1000) / 100;
				 int d3 = (steps - d1 * 1000 - d2 * 100) / 10;
				 int d4 = steps % 10;
				 
				 setStepsCount[2] = this->ydriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';
				 setStepsCount[7] = static_cast<char>(d3) + '0';
				 setStepsCount[8] = static_cast<char>(d4) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else if (steps / 100)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', '0', 0x0D};

				 int d1 = steps / 100;
				 int d2 = (steps - d1 * 100) / 10;
				 int d3 = steps % 10;
				 
				 setStepsCount[2] = this->ydriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';
				 setStepsCount[7] = static_cast<char>(d3) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else if (steps / 10)
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', '0', 0x0D};

				 int d1 = steps / 10;
				 int d2 = steps % 10;

				 setStepsCount[2] = this->ydriver;
				 setStepsCount[5] = static_cast<char>(d1) + '0';
				 setStepsCount[6] = static_cast<char>(d2) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 }
			 else
			 {
				 char setStepsCount[] = {':', '0', '0', 'S', 'C', '0', 0x0D};


				 setStepsCount[2] = this->ydriver;
				 setStepsCount[5] = static_cast<char>(steps) + '0';

				 String^ stringToDisplay = gcnew String(setStepsCount);
				 this->SetLabel5Text(stringToDisplay + "\n" + System::Convert::ToString(sizeof(setStepsCount)));

				 iRet = WriteFile(hSerial, setStepsCount, sizeof(setStepsCount), &iSize, NULL);
				 Sleep(500);
				 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
			 };
		 }
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->moveLeft();
		}
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->moveUp();
		 }
private: System::Void moveLeft()
		 {
			char selectFixedStepRegime[] = {':', '0', '0', 'S', 'E', '1', 0x0D};
			char makeMovement[] = {':', '0', '0', 'R', '0', 0x0D};
			char driverAnswer[20];

			selectFixedStepRegime[2] = this->xdriver;
			makeMovement[2] = this->xdriver;
			makeMovement[4] = this->xleft;

			BOOL iRet;
			DWORD iSize;

			this->xstepsDone -= this->xStepsManual;

			iRet = WriteFile(hSerial, makeMovement, sizeof(makeMovement), &iSize, NULL);
			Sleep(500);
			iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
			this->stepsInfoUpdate();
		 }
private: System::Void moveUp()
		 {
			 char selectFixedStepRegime[] = {':', '0', '0', 'S', 'E', '1', 0x0D};
			 char makeMovement[] = {':', '0', '0', 'R', '0', 0x0D};
			 char driverAnswer[20];

			 BOOL iRet;
			 DWORD iSize;

			 this->ystepsDone += this->yStepsManual;

			 selectFixedStepRegime[2] = this->ydriver;
			 makeMovement[2] = this->ydriver;
			 makeMovement[4] = this->yup;

			 iRet = WriteFile(hSerial, makeMovement, sizeof(makeMovement), &iSize, NULL);
			 Sleep(200);
			 iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
			 this->stepsInfoUpdate();
		}
private: System::Void moveRight()
		 {
			char selectFixedStepRegime[] = {':', '0', '0', 'S', 'E', '1', 0x0D};
			char makeMovement[] = {':', '0', '0', 'R', '0', 0x0D};
			char driverAnswer[20];

			selectFixedStepRegime[2] = this->xdriver;
			makeMovement[2] = this->xdriver;
			makeMovement[4] = this->xright;

			BOOL iRet;
			DWORD iSize;

			this->xstepsDone += this->xStepsManual;

			iRet = WriteFile(hSerial, makeMovement, sizeof(makeMovement), &iSize, NULL);
			Sleep(500);
			iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
			this->stepsInfoUpdate();
		 }
private: System::Void moveDown()
		 {
			 char selectFixedStepRegime[] = {':', '0', '0', 'S', 'E', '1', 0x0D};
			 char makeMovement[] = {':', '0', '0', 'R', '0', 0x0D};
			 char driverAnswer[20];

			 BOOL iRet;
			 DWORD iSize;

			 this->ystepsDone -= this->yStepsManual;

			 selectFixedStepRegime[2] = this->ydriver;
			 makeMovement[2] = this->ydriver;
			 makeMovement[4] = this->ydown;

			 iRet = WriteFile(hSerial, makeMovement, sizeof(makeMovement), &iSize, NULL);
			 Sleep(200);
			 iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
			 this->stepsInfoUpdate();
		 }

private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->moveRight();
		 }
private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->moveDown();
		 }
private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
			 if (this->_isStarted)
			 {
				 return;
			 }

			 switch(e->KeyCode)
			 {
			 case Keys::A: 
					 this->button8->PerformClick();
					 e->Handled = true;
					break;
			 case Keys::W:
					 this->button9->PerformClick();
					 e->Handled = true;
					 break;
			 case Keys::D:
					 this->button10->PerformClick();
					 e->Handled = true;
					 break;
				 case Keys::S:
					 this->button11->PerformClick();
					 e->Handled = true;
					 break;
				 case Keys::Escape:
					 this->button16->PerformClick();
					 e->Handled = true;
					 break;
			 }
		 }
private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
			 System::Windows::Forms::DialogResult result = this->folderBrowserDialog1->ShowDialog();
			 if (result == System::Windows::Forms::DialogResult::OK)
			 {
				 this->folderName = this->folderBrowserDialog1->SelectedPath + L"\\" +  DateTime::Now.ToString("yyyyMMdd");;
				 this->_isFolderCreated = false;
			 }
		 }
private: System::Void Form1_PreviewKeyDown(System::Object^  sender, System::Windows::Forms::PreviewKeyDownEventArgs^  e) {
			 if (this->_isStarted)
			 {
				 return;
			 }
			

			 switch(e->KeyCode)
			 {
				 case Keys::A: 
					 this->button8->PerformClick();
					 //
					break;
				 case Keys::W:
					 this->button9->PerformClick();
					 break;
				 case Keys::D:
					 this->button10->PerformClick();
					 break;
				 case Keys::S:
					 this->button11->PerformClick();
					 break;
				 case Keys::Escape:
					 this->button16->PerformClick();
					 break;
			 }
		 }
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->fileNameBase = this->textBox1->Text;
		 }
private: void saveToBITMAP(unsigned char *data, int vertical_size, int horizontal_size) {
			 System::IO::StreamWriter^ sw = gcnew System::IO::StreamWriter(this->currentOutFileName);
			 int x = 0;
			 int y = 0;

			 for (x = 0; x < horizontal_size; x++)			// BMP image format is written from bottom to top...
			 {
				for (y = 0; y < vertical_size; y++)
				{
					unsigned char color = data[x * vertical_size + y];
					
					sw->Write(System::Convert::ToString(color)+ L"\t");
				}
				sw->Write(L"\n");
			}

			 sw->Close();
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!this->_isFolderCreated)
			 {
				 if (System::IO::Directory::Exists(this->folderName))
				 {
					 String ^auxFolders = this->folderName + L"//frames";
					 String ^temp;
					 if (System::IO::Directory::Exists(auxFolders))
					 {
						 while(true)
						 {
							 temp = this->folderName + L"//frames//" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
								+ "_small.dat";
							 if (System::IO::File::Exists(temp))
							 {
								 this->storedImageCounterSmall++;
							 }
							 else
							 {
								 break;
							 }
						 }

						  while(true)
						 {
							 temp = this->folderName + L"//frames//" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
								+ "_small.dat";
							 if (System::IO::File::Exists(temp))
							 {
								 this->storedImageCounterSmall++;
							 }
							 else
							 {
								 break;
							 }
						 }
					 }
					 else
					 {
						System::IO::DirectoryInfo ^di = System::IO::Directory::CreateDirectory(auxFolders);
						this->storedImageCounterLarge = 0;
						this->storedImageCounterSmall = 0;
					 }

					 auxFolders = this->folderName + L"//statistics";

					 if (!System::IO::Directory::Exists(auxFolders))
					 {
						 System::IO::DirectoryInfo ^di = System::IO::Directory::CreateDirectory(auxFolders);
					 }
				 }
				 else
				 {
					 System::IO::DirectoryInfo ^di = System::IO::Directory::CreateDirectory(this->folderName);
					 System::IO::DirectoryInfo ^d1 = System::IO::Directory::CreateDirectory(this->folderName + L"//frames");
					 System::IO::DirectoryInfo ^d2 = System::IO::Directory::CreateDirectory(this->folderName + L"//statistics");
					 this->storedImageCounterLarge = 0;
					 this->storedImageCounterSmall = 0;
				 }
			 }
			 this->currentOutFileName = this->folderName + L"//frames//" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
				 + "_small.dat";
			 
			 this->printFrameStaticstic();
			 this->saveToBITMAP(this->tdata, static_cast<int>(this->_roi_size_v), static_cast<int>(this->_roi_size_h));
			 this->storedImageCounterSmall++;
		 }

private: System::Void button16_Click(System::Object^  sender, System::EventArgs^  e) {
			 char stopDriver[] = {':', '0', '1', 'R', 'S', 0x0D};
			 BOOL iRet;
			 DWORD iSize;
			 char driverAnswer[13];

			 iRet = WriteFile(hSerial, stopDriver, sizeof(stopDriver), &iSize, NULL);
			 Sleep(100);
			 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);

			 stopDriver[2] = '2';

			 iRet = WriteFile(hSerial, stopDriver, sizeof(stopDriver), &iSize, NULL);
			 Sleep(100);
			 iRet = ReadFile(hSerial, driverAnswer, 7, &iSize, NULL);
		 }
private: System::Void moveToTheScanningOrigin()
		 {
			 char moveXDriver[] = {':', '0', '0', 'R', '0', 0x0D};
			 char moveYDriver[] = {':', '0', '0', 'R', '0', 0x0D};
			 char driverAnswer[13];
			 BOOL iRet;
			 DWORD iSize;

			 moveXDriver[2] = this->xdriver;
			 moveYDriver[2] = this->ydriver;
			 if (this->xstepsDone != 0)
			 {
				 this->xstepsDone = 0;
				if (this->xstepsDone > 0)
				{
					 moveXDriver[4] = this->xleft;
				}
				else
				{
					 moveXDriver[4] = this->xright;
				}
				int stepsToReturn = abs(this->xstepsDone);
				if (stepsToReturn > 1)
				{
					this->setXSteps(stepsToReturn);

					iRet = WriteFile(hSerial, moveXDriver, sizeof(moveXDriver), &iSize, NULL);
					Sleep(200);
					iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
				}
				else
				{
					this->setXSteps(3);
					
					iRet = WriteFile(hSerial, moveXDriver, sizeof(moveXDriver), &iSize, NULL);
					Sleep(200);
					iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);

					this->setXSteps(2);

					if (moveXDriver[4] == this->xleft)
					{
						moveXDriver[4] = this->xright;
					}
					else
					{
						moveXDriver[4] = this->xleft;
					}
					iRet = WriteFile(hSerial, moveXDriver, sizeof(moveXDriver), &iSize, NULL);
					Sleep(200);
					iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
				}

				if (this->ystepsDone != 0)
			    {
					this->ystepsDone = 0;
					if (this->ystepsDone > 0)
					{
						 moveYDriver[4] = this->ydown;
					}
					else
					{
						 moveYDriver[4] = this->yup;
					}
					int stepsToReturn = abs(this->ystepsDone);
					if (stepsToReturn > 1)
					{
						this->setYSteps(stepsToReturn);

						iRet = WriteFile(hSerial, moveYDriver, sizeof(moveYDriver), &iSize, NULL);
						Sleep(200);
						iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
					}
					else
					{
						this->setXSteps(3);
						
						iRet = WriteFile(hSerial, moveYDriver, sizeof(moveYDriver), &iSize, NULL);
						Sleep(200);
						iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);

						this->setYSteps(2);

						if (moveYDriver[4] == this->ydown)
						{
							moveYDriver[4] = this->yup;
						}
						else
						{
							moveYDriver[4] = this->ydown;
						}
						iRet = WriteFile(hSerial, moveYDriver, sizeof(moveYDriver), &iSize, NULL);
						Sleep(200);
						iRet = ReadFile(hSerial, driverAnswer, 13, &iSize, NULL);
					}
				}

			}
			 this->stepsInfoUpdate();
		 }
private: System::Void stepsInfoUpdate()
		 {
			 String^ text = L"Steps done: \n" + L"x: " + System::Convert::ToString(this->xstepsDone) + L"\n"
				 + L"y: " + System::Convert::ToString(this->ystepsDone);
			 this->SetLabel11Text(text);
		 }
private: System::Void disableButtons()
		 {
			 this->numericUpDown1->Enabled = false;
			 this->numericUpDown2->Enabled = false;
			 this->numericUpDown3->Enabled = false;
			 this->numericUpDown4->Enabled = false;
			 this->numericUpDown5->Enabled = false;
			 this->button7->Enabled = false;
			 this->button13->Enabled = false;
			 this->radioButton1->Enabled = false;
			 this->radioButton2->Enabled = false;
			 this->radioButton3->Enabled = false;
			 this->radioButton4->Enabled = false;
			 this->radioButton5->Enabled = false;
			 this->radioButton6->Enabled = false;
			 this->radioButton7->Enabled = false;
			 this->radioButton8->Enabled = false;
			 this->button2->Enabled = true;
			 this->button8->Enabled = false;
			 this->button9->Enabled = false;
			 this->button10->Enabled = false;
			 this->button11->Enabled = false;
			 this->button12->Enabled = false;
			 this->button14->Enabled = false;
		 }
		 
private: System::Void enableButtons()
		 {
			 this->numericUpDown1->Enabled = true;
			 this->numericUpDown2->Enabled = true;
			 this->numericUpDown3->Enabled = true;
			 this->numericUpDown4->Enabled = true;
			 this->numericUpDown5->Enabled = true;
			 this->button7->Enabled = true;
			 this->button13->Enabled = true;
			 this->radioButton1->Enabled = true;
			 this->radioButton2->Enabled = true;
			 this->radioButton3->Enabled = true;
			 this->radioButton4->Enabled = true;
			 this->radioButton5->Enabled = true;
			 this->radioButton6->Enabled = true;
			 this->radioButton7->Enabled = true;
			 this->radioButton8->Enabled = true;
			 this->button2->Enabled = true;
			 this->button8->Enabled = true;
			 this->button9->Enabled = true;
			 this->button10->Enabled = true;
			 this->button11->Enabled = true;
			 this->button12->Enabled = true;
			 this->button14->Enabled = true;
		 }
private: System::Void printFrameStaticstic()
		 {
			bool found = false;
			bool highIntFlag = false;
			int size_h = static_cast<int>(this->_roi_size_h);
			int size_v = static_cast<int>(this->_roi_size_v);
			int *temporary = new int [size_h * size_v];
			int *highInt = new int [size_h * size_v];
			int *mask = new int [size_h * size_v];
			int i = 0;
			int j = 0;
			int maxIntensity = 0;

			for (i  = 0; i < size_h; i++)
			{
				for (j = 0; j < size_v; j++)
				{
					temporary[i * size_v + j] = static_cast<int>(this->tdata[i * size_v + j]);
					if (maxIntensity < temporary[i * size_v + j])
					{
						maxIntensity = temporary[i * size_v +j];
					}
				}
			}
			
			for (i = 0; i < size_h; i++)
			{
				for (j = 0; j < size_v; j++)
				{
					highInt[i * size_v + j] = (temporary[i * size_v + j] < static_cast<int>(this->cutOff * maxIntensity)) ? (1) : (0);
					if (highInt[ i * size_v + j] && this->background[i * size_v + j])
					{
						highInt[i * size_v + j] = 0;
					}
					mask[i * size_v + j] = 0;
				}
			}

			int globalIslandIndex = 1;
			int currArea = 0;
			int totalArea = 0;
			std::map<int, int> areas;

			for (i = 0; i < size_h; i++)
			{
				for (j = 0; j < size_v; j++)
				{
					currArea = 0;
					if ((highInt[i * size_v + j]) && (!mask[i * size_v + j]))
					{
						currArea++;
						highIntFlag = true;
						bool reached = false;
						mask[i * size_v + j] = globalIslandIndex;
						std::vector<std::pair<int, int>> nextPoints;
						std::pair<int, int> tPoint(i, j);
						nextPoints.push_back(tPoint);

						while (!reached)
						{
							int added = 0;
							int nl = nextPoints.size();
							for (int ct = 0; ct < nl; ct++)
							{
								int ic = nextPoints[ct].first;
								int jc = nextPoints[ct].second;
								if ((ic - 1) > -1)
								{
									if ((highInt[(ic - 1) * size_v + jc]) && (!mask[(ic - 1) * size_v + jc]))
									{
										tPoint.first = ic - 1;
										tPoint.second = jc;
										nextPoints.push_back(tPoint);
										mask[(ic - 1) * size_v + jc] = globalIslandIndex;
										currArea++;
										added++;
									}
								}
								if ((ic + 1) < size_h)
								{
									if((highInt[(ic + 1) * size_v + jc]) && (!mask[(ic + 1) * size_v + jc]))
									{
										tPoint.first = ic + 1;
										tPoint.second = jc;
										nextPoints.push_back(tPoint);
										mask[(ic + 1) * size_v + jc] = globalIslandIndex;
										currArea++;
										added++;
									}
								}
								if ((jc - 1) > -1)
								{
									if ((highInt[ic * size_v + jc - 1]) && (!mask[ic * size_v + jc - 1]))
									{
										tPoint.first = ic;
										tPoint.second = jc - 1;
										nextPoints.push_back(tPoint);
										mask[ic * size_v + jc - 1] = globalIslandIndex;
										currArea++;
										added++;
									}
								}
								if ((jc + 1) < size_v)
								{
									if ((highInt[ic * size_v + jc + 1]) && (!mask[ic * size_v + jc + 1]))
									{
										tPoint.first = ic;
										tPoint.second = jc + 1;
										nextPoints.push_back(tPoint);
										mask[ic * size_v + jc + 1] = globalIslandIndex;
										currArea++;
										added++;
									}
								}
							}
							nextPoints.erase(nextPoints.begin(), nextPoints.begin() + nl);

							if(added == 0)
							{
								reached = true;
							}
												
						}
						totalArea += currArea;
						globalIslandIndex++;
						if (areas.count(currArea) == 0)
						{
							areas.insert(std::make_pair(currArea, 1));
						}
						else
						{
							areas[currArea] += 1;
						}
					}
				}
			}


			String^ statisticOutFileName = this->folderName + L"//statistics//" + L"stat_" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
				 + "_small.dat";
			String^ mapOutFileName = this->folderName + L"//" + L"map_" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
				 + "_small.dat";
			String^ maskOutFileName = this->folderName + L"//" + L"mask_" + this->fileNameBase + System::Convert::ToString(this->storedImageCounterSmall)
				 + "_small.dat";

			double percentage = static_cast<double>(totalArea) / static_cast<double>(size_v * size_h);
			double averageArea = (globalIslandIndex > 0) ? (static_cast<double>(totalArea) / static_cast<double>(globalIslandIndex)) : 0.0;

			System::IO::StreamWriter^ sw = gcnew System::IO::StreamWriter(statisticOutFileName);
			sw->Write(L"" + System::Convert::ToString(globalIslandIndex) + L"\n");
			sw->Write(L"" + System::Convert::ToString(totalArea) + L"\n");
			sw->Write(L"" + System::Convert::ToString(percentage) + L"\n");
			sw->Write(L"" + System::Convert::ToString(averageArea) + L"\n");

			std::map<int,int>::iterator it;

			for (it = areas.begin(); it != areas.end(); it++)
			{
				sw->Write(System::Convert::ToString(it->first) + L"\t" + System::Convert::ToString(it->second) + L"\n");
			}

			sw->Close();

			sw = gcnew System::IO::StreamWriter(mapOutFileName);

			for (i = 0; i < size_h; i++)
			{
				for (j = 0; j < size_v; j++)
				{
					sw->Write(System::Convert::ToString(mask[i * size_v + j]) + L"\t");
				}
				sw->Write(L"\n");
			}

			sw->Close();

			sw = gcnew System::IO::StreamWriter(maskOutFileName);

			for (i = 0; i < size_h; i++)
			{
				for (j = 0; j < size_v; j++)
				{
					sw->Write(System::Convert::ToString(128 * highInt[i * size_v + j]) + L"\t");
				}
				sw->Write(L"\n");
			}

			sw->Close();

			delete [] highInt;
			delete [] mask;
			delete [] temporary;

		 }

		 
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 int size_h = static_cast<int>(this->_roi_size_h);
			 int size_v = static_cast<int>(this->_roi_size_v);
			 int *temporary = new int [size_h * size_v];
			 int i = 0;
			 int j = 0;
			 int maxIntensity = 0;

			 for (i  = 0; i < size_h; i++)
			 {
				for (j = 0; j < size_v; j++)
				{
					temporary[i * size_v + j] = static_cast<int>(this->tdata[i * size_v + j]);
					if (maxIntensity < temporary[i * size_v + j])
					{
						maxIntensity = temporary[i * size_v +j];
					}
				}
			 }
			 
			 for (i = 0; i < size_h; i++)
			 {
				 for (j = 0; j < size_v; j++)
				 {
					this->background[i * size_v + j] = (temporary[i * size_v + j] < static_cast<int>(this->cutOff * maxIntensity)) ? (1) : (0);
				 }
			 }

			 delete [] temporary;
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
};
};



