import numpy as np
from numpngw import write_png

file_name_base = "Release\\20180316\\frames\\scan_"

i = 0

while i < 8:
    nx = 0
    ny = 0
    img_data = []
    file_name = file_name_base + str(i) + "_small.dat"
    print(file_name)
    with open(file_name, "r") as fin:
        for line in fin:
            tlist = [int(x) for x in line.split()]
            ny = len(tlist)
            nx += 1
            img_data.extend(tlist)
    img_to_output = np.array(img_data, dtype=np.uint8).reshape(nx, ny)
    test_img = np.fliplr(img_to_output)
    img_name = "scan_" + str(i) + ".png"
    write_png(img_name, img_to_output, bitdepth=8)
    i += 1
    